<?php
namespace Ponay\Test\User\Command\Handler;

use Ponay\Ponay\App;
use Ponay\Ponay\User\Command\BanUser;
use Ponay\Ponay\User\Command\EmailLogin as EmailLoginCommand;
use Ponay\Ponay\User\Command\EmailRegister;
use Zend\ServiceManager\ServiceLocatorInterface;

class EmailLoginTest extends \PHPUnit_Framework_TestCase
{
    protected $userName;
    protected $password;
    protected $firstName;
    protected $lastName;
    protected $email;
    /** @var  ServiceLocatorInterface */
    protected $sm;

    /**
     * @test
     * @expectedException \Ponay\Domain\Command\Exception\InvalidCommandArgumentException
     */
    public function givenCommandWithoutPassword_willThrowException()
    {
        $this->registerUser();
        $command = new EmailLoginCommand($this->userName, null);
        $this->sm->get('command.bus')->execute($command);
    }

    /**
     * @test
     * @expectedException \Ponay\Domain\Command\Exception\InvalidCommandArgumentException
     */
    public function givenCommandWithoutUserName_willThrowException()
    {
        $this->registerUser();
        $command = new EmailLoginCommand(null, $this->password);
        $this->sm->get('command.bus')->execute($command);
    }

    /**
     * @test
     */
    public function givenCorrectUsernamePassword_returnsUserEntity()
    {
        $this->registerUser();
        $command = new EmailLoginCommand($this->userName, $this->password);
        $user = $this->sm->get('command.bus')->execute($command);
        $this->assertInstanceOf('Ponay\Ponay\User\UserEntity', $user);
    }

    /**
     * @test
     * @expectedException \Ponay\Ponay\User\Exception\Login\BannedUserException
     */
    public function givenUserIsBanned_willThrowException()
    {
        $user = $this->registerUser();
        $banUserCommand = new BanUser($user);
        $this->sm->get('command.bus')->execute($banUserCommand);

        $command = new EmailLoginCommand($this->userName, $this->password);
        $this->sm->get('command.bus')->execute($command);
    }

    /**
     * @test
     * @expectedException \Ponay\Ponay\User\Exception\Login\PasswordNotMatchedException
     */
    public function givenWrongPassword_willThrowException()
    {
        $this->registerUser();
        $command = new EmailLoginCommand($this->userName, 'wrong password');
        $this->sm->get('command.bus')->execute($command);
    }

    /**
     * @test
     * @expectedException \Ponay\Ponay\User\Exception\Login\UsernameNotFoundException
     */
    public function givenWrongUserName_willThrowException()
    {
        $this->registerUser();
        $command = new EmailLoginCommand('wrong username', $this->password);
        $this->sm->get('command.bus')->execute($command);
    }

    public function setUp()
    {
        parent::setUp();
        $this->userName = 'username';
        $this->password = 'password';
        $this->firstName = 'first name';
        $this->lastName = 'last name';
        $this->email = 'email@example.com';

        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }

    private function registerUser()
    {
        $command = new EmailRegister($this->userName, $this->firstName, $this->lastName, $this->password, $this->password, $this->email);
        $user = $this->sm->get('command.bus')->execute($command);

        return $user;
    }
}