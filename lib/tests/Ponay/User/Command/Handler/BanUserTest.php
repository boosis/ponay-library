<?php
namespace Ponay\Test\User\Command\Handler;

use Ponay\Ponay\App;
use Ponay\Ponay\User\Command\BanUser;
use Ponay\Ponay\User\Command\EmailLogin;
use Ponay\Ponay\User\Command\EmailRegister;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class BanUserTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;

    /**
     * @test
     * @expectedException \Ponay\Ponay\User\Exception\Login\BannedUserException
     */
    public function givenUserIsBanned_loginWillThrowException()
    {
        /** @var UserEntity $user */
        $user = $this->registerUser();

        $command = new BanUser($user);
        $user = $this->sm->get('command.bus')->execute($command);

        $loginCommand = new EmailLogin($user->getUserName(), 'password');
        $this->sm->get('command.bus')->execute($loginCommand);
    }

    /**
     * @test
     */
    public function givenUserIsBanned_statusWillBeBanned()
    {
        /** @var UserEntity $user */
        $user = $this->registerUser();
        $command = new BanUser($user);
        $user = $this->sm->get('command.bus')->execute($command);
        $this->assertTrue($user->isBanned());

    }

    public function setUp()
    {
        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }

    private function registerUser()
    {
        $command = new EmailRegister('username', 'first name', 'last name', 'password', 'password', 'boo@example.com');
        $user = $this->sm->get('command.bus')->execute($command);

        return $user;
    }
}