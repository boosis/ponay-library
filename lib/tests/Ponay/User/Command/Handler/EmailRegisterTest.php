<?php
namespace Ponay\Test\User\Command\Handler;

use Ponay\Ponay\App;
use Ponay\Ponay\User\Command\EmailRegister;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class EmailRegisterTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;

    /**
     * @test
     */
    public function givenCorrectInformation_willReturnUserEntity()
    {
        $command = new EmailRegister('username', 'first name', 'last name', 'password', 'password', 'email@example.com');
        /** @var UserEntity $user */
        $user = $this->sm->get('command.bus')->execute($command);
        $this->assertEquals($command->username, $user->getUserName());
    }

    /**
     * @test
     * @expectedException \Ponay\Ponay\User\Exception\Register\EmailAlreadyRegisteredException
     */
    public function givenEmailAlreadyRegistered_willThrowException()
    {
        $command = new EmailRegister('username', 'first name', 'last name', 'password', 'password', 'email@example.com');
        $this->sm->get('command.bus')->execute($command);

        $command = new EmailRegister('username2', 'first name', 'last name', 'password', 'password', 'email@example.com');
        $this->sm->get('command.bus')->execute($command);
    }

    /**
     * @test
     * @expectedException \Ponay\Domain\Command\Exception\InvalidCommandArgumentException
     */
    public function givenMissingInformationInCommand_willThrowException()
    {
        $command = new EmailRegister(null, 'first name', 'last name', 'password', 'password', 'email@example.com');
        $this->sm->get('command.bus')->execute($command);
    }

    /**
     * @test
     * @expectedException \Ponay\Ponay\User\Exception\Register\UsernameTakenException
     */
    public function givenUserNameAlreadyUsed_willThrowException()
    {
        $command = new EmailRegister('username', 'first name', 'last name', 'password', 'password', 'email@example.com');
        $this->sm->get('command.bus')->execute($command);

        $command = new EmailRegister('username', 'first name', 'last name', 'password', 'password', 'email@example.com');
        $this->sm->get('command.bus')->execute($command);
    }

    public function setUp()
    {
        parent::setUp();
        $this->userName = 'bill';
        $this->password = 'password';

        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }
}