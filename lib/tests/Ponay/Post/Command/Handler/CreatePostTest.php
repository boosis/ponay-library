<?php
namespace Ponay\Test\Ponay\Post\Command\Handler;

use Ponay\Domain\Event\Event;
use Ponay\Domain\Event\EventDispatcher;
use Ponay\Ponay\App;
use Ponay\Ponay\Post\Command\CreatePost as CreatePostCommand;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class CreatePostTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;

    /**
     * @test
     */
    public function can_reply_to_a_post()
    {
        $author = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($author);

        /** @var EventDispatcher $eventBus */
        $eventBus = $this->sm->get('event.bus');
        $eventBus->addListener(
            'Ponay\Ponay\Post\Event\PostCreated',
            function (Event $event) {
                /** @var PostEntity $post */
                $post = $event->getParam('post');
                $this->assertInstanceOf('Ponay\Ponay\Post\PostEntity', $post);
            });
        $eventBus->addListener(
            'Ponay\Ponay\Post\Event\PostReplied',
            function (Event $event) {
                /** @var PostEntity $post */
                $post = $event->getParam('post');
                /** @var PostEntity $replyTo */
                $replyTo = $event->getParam('replyTo');
                $this->assertEquals($post->replyToId(), $replyTo->getId());
            });
        $command = new CreatePostCommand($author, 'title', 'short description', 'description', ['tag1', 'tag2']);
        /** @var PostEntity $post */
        $post = $this->sm->get('command.bus')->execute($command);

        $command = new CreatePostCommand($author, 'reply to post 1', 'short description for the reply', 'description for the reply', ['tag3', 'tag4'], $post);
        /** @var PostEntity $reply */
        $reply = $this->sm->get('command.bus')->execute($command);

        $this->assertEquals($post->getId(), $reply->replyToId());
    }

    /**
     * @test
     */
    public function givenCreatePostCommand_returnsPostModel()
    {
        $author = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($author);

        $command = new CreatePostCommand($author, 'title', 'short description', 'description', ['tag1', 'tag2']);
        /** @var PostEntity $post */
        $post = $this->sm->get('command.bus')->execute($command);
        $this->assertInstanceOf('\Ponay\Ponay\Post\PostEntity', $post);
        $this->assertFalse($post->isDeleted());
    }

    /**
     * @test
     */
    public function givenNotDescription_shouldThrowException()
    {
        $author = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($author);

        $this->setExpectedException('\Ponay\Domain\Command\Exception\InvalidCommandArgumentException');
        $command = new CreatePostCommand($author, 'title', 'short description', '', ['tag1', 'tag2']);
        $post = $this->sm->get('command.bus')->execute($command);
        $this->assertInstanceOf('\Ponay\Ponay\Post\PostEntity', $post);
    }

    /**
     * @test
     */
    public function givenNotTitle_shouldThrowException()
    {
        $author = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($author);

        $this->setExpectedException('\Ponay\Domain\Command\Exception\InvalidCommandArgumentException');
        $command = new CreatePostCommand($author, null, 'short description', 'description', ['tag1', 'tag2']);
        $post = $this->sm->get('command.bus')->execute($command);
        $this->assertInstanceOf('\Ponay\Ponay\Post\PostEntity', $post);
    }

    public function setUp()
    {
        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }
} 