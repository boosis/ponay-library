<?php
namespace Ponay\Test\Ponay\Post\Command\Handler;


use Ponay\Domain\Event\Event;
use Ponay\Domain\Event\EventDispatcher;
use Ponay\Domain\Repository\BaseRepository;
use Ponay\Ponay\App;
use Ponay\Ponay\Post\Command\CreatePost as CreatePostCommand;
use Ponay\Ponay\Post\Command\EditPost as EditPostCommand;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class EditPostTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;

    /**
     * @test
     */
    public function editPostWillTriggerPostEditedEventWithChangedValues()
    {
        $author = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($author);

        /** @var EventDispatcher $eventBus */
        $eventBus = $this->sm->get('event.bus');
        $eventBus->addListener(
            'Ponay\Ponay\Post\Event\PostEdited',
            function (Event $event) {
                $changes = $event->getParam('changes');
                $this->assertEquals('new title', $changes['t']);
            });

        $command = new CreatePostCommand($author, 'title', 'short description', 'description', ['tag1', 'tag2']);
        /** @var PostEntity $originalPost */
        $originalPost = $this->sm->get('command.bus')->execute($command);

        $command = new EditPostCommand($originalPost, 'new title', 'new short description', 'new description', ['new tag1']);
        $this->sm->get('command.bus')->execute($command);
    }

    /**
     * @test
     */
    public function givenEditPostCommand_returnsPostModelWithChangedFields()
    {
        $author = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($author);

        $command = new CreatePostCommand($author, 'title', 'short description', 'description', ['tag1', 'tag2']);
        /** @var PostEntity $originalPost */
        $originalPost = $this->sm->get('command.bus')->execute($command);

        $command = new EditPostCommand($originalPost, 'new title', 'new short description', 'new description', ['new tag1']);
        $this->sm->get('command.bus')->execute($command);

        /** @var BaseRepository $repo */
        $repo = $this->sm->get('postrepo');
        /** @var PostEntity $post */
        $post = $repo->getById($originalPost->getId());

        $this->assertEquals($originalPost->getId(), $post->getId());
        $this->assertEquals('new title', $post->getTitle());
    }

    public function setUp()
    {
        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }
} 