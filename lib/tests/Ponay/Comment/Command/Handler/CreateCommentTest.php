<?php
namespace Ponay\Test\Ponay\Comment\Command\Handler;

use Ponay\Domain\Repository\RepositoryLocator;
use Ponay\Ponay\App;
use Ponay\Ponay\Comment\Command\CreateComment as CreateCommentCommand;
use Ponay\Ponay\Comment\CommentEntity;
use Ponay\Ponay\Comment\Enum\CommentableType;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class CreateCommentTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;
    /** @var  RepositoryLocator */
    protected $rm;

    /**
     * @test
     */
    public function can_create_comment()
    {
        $commentable = PostEntity::factory([]);
        $this->rm->get($commentable)->save($commentable);

        $commenter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($commenter)->save($commenter);

        $command = new CreateCommentCommand($commentable, $commenter, CommentableType::POST, 'Test comment');
        $comment = $this->sm->get('command.bus')->execute($command);

        $this->assertInstanceOf(CommentEntity::class, $comment);
    }

    /**
     * @test
     */
    public function can_reply_to_a_comment()
    {
        $commentable = PostEntity::factory([]);
        $this->rm->get($commentable)->save($commentable);

        $commenter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($commenter)->save($commenter);

        $command = new CreateCommentCommand($commentable, $commenter, CommentableType::POST, 'Test comment');
        /** @var CommentEntity $comment */
        $comment = $this->sm->get('command.bus')->execute($command);

        $command = new CreateCommentCommand($comment, $commenter, CommentableType::COMMENT, 'Test comment');
        /** @var CommentEntity $reply */
        $reply = $this->sm->get('command.bus')->execute($command);

        $this->assertEquals($comment->getId(), $reply->getSubjectId());
        $this->assertTrue($reply->isReply());
    }

    /**
     * @test
     */
    public function givenCommentIsCreated_allParentsCommentCountShouldIncrease()
    {
        $commentable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($commentable);

        $commenter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($commenter);

        $command = new CreateCommentCommand($commentable, $commenter, CommentableType::POST, 'Test comment');
        /** @var CommentEntity $comment */
        $comment = $this->sm->get('command.bus')->execute($command);

        $command = new CreateCommentCommand($comment, $commenter, CommentableType::COMMENT, 'Test comment');
        /** @var CommentEntity $comment */
        $reply = $this->sm->get('command.bus')->execute($command);

        $command = new CreateCommentCommand($comment, $commenter, CommentableType::COMMENT, 'Test comment');
        /** @var CommentEntity $comment */
        $reply2 = $this->sm->get('command.bus')->execute($command);

        $command = new CreateCommentCommand($reply, $commenter, CommentableType::COMMENT, 'Test comment');
        /** @var CommentEntity $comment */
        $replyreply = $this->sm->get('command.bus')->execute($command);

        $this->assertEquals(4, $this->rm->get($commentable)->getById($commentable->getId())->getCommentCount());
        $this->assertEquals(3, $this->rm->get($comment)->getById($comment->getId())->getCommentCount());
        $this->assertEquals(1, $this->rm->get($reply)->getById($reply->getId())->getCommentCount());
        $this->assertEquals(0, $this->rm->get($reply2)->getById($reply2->getId())->getCommentCount());
        $this->assertEquals(0, $this->rm->get($replyreply)->getById($replyreply->getId())->getCommentCount());
    }

    /**
     * @test
     */
    public function givenCommentableIsComment_replyCountWillIncrease()
    {
        $commentable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($commentable);

        $commenter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($commenter);

        $command = new CreateCommentCommand($commentable, $commenter, CommentableType::POST, 'Test comment');
        /** @var CommentEntity $comment */
        $comment = $this->sm->get('command.bus')->execute($command);

        $command = new CreateCommentCommand($comment, $commenter, CommentableType::COMMENT, 'Test comment');
        $this->sm->get('command.bus')->execute($command);

        $comment = $this->sm->get('commentrepo')->getById($comment->getId());
        $this->assertEquals(1, $comment->getCommentCount());

        $command = new CreateCommentCommand($comment, $commenter, CommentableType::COMMENT, 'Test comment');
        $this->sm->get('command.bus')->execute($command);

        $comment = $this->sm->get('commentrepo')->getById($comment->getId());
        $this->assertEquals(2, $comment->getCommentCount());
    }

    public function setUp()
    {
        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
        $this->rm = $this->sm->get('repomanager');
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }
}