<?php
namespace Ponay\Test\Ponay\Like\Command\Handler;

use Ponay\Domain\Command\CommandBus;
use Ponay\Ponay\App;
use Ponay\Ponay\Like\Command\Like as LikeCommand;
use Ponay\Ponay\Like\Command\Unlike as UnlikeCommand;
use Ponay\Ponay\Like\Enum\LikeableType;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class UnlikeTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;
    /** @var  CommandBus */
    protected $bus;

    /**
     * @test
     */
    public function can_unlike_liked_likable()
    {
        $liker = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($liker);

        $likeable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($likeable);

        $command = new LikeCommand($liker, $likeable, LikeableType::USER);
        $this->sm->get('command.bus')->execute($command);

        $command = new UnlikeCommand($liker, $likeable, LikeableType::USER);
        $result = $this->sm->get('command.bus')->execute($command);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function liking_post_will_increase_like_count()
    {

        $liker = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($liker);

        $likeable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($likeable);

        $command = new LikeCommand($liker, $likeable, LikeableType::USER);
        $this->sm->get('command.bus')->execute($command);

        $this->assertEquals(1, $likeable->getLikeCount());

        $command = new UnlikeCommand($liker, $likeable, LikeableType::USER);
        $this->sm->get('command.bus')->execute($command);

        $this->assertEquals(0, $likeable->getLikeCount());
    }

    public function setUp()
    {
        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }

    /**
     * @test
     */
    public function unlike_unliked_will_throw_exception()
    {
        $this->setExpectedException('Ponay\Ponay\Like\Command\Exception\NotLikedException');
        $liker = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($liker);

        $likeable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($likeable);

        $command = new UnlikeCommand($liker, $likeable, LikeableType::USER);
        $this->sm->get('command.bus')->execute($command);
    }
}