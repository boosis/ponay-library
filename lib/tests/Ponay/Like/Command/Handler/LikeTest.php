<?php
namespace Ponay\Test\Ponay\Like\Command\Handler;


use Ponay\Ponay\App;
use Ponay\Ponay\Like\Command\Like as LikeCommand;
use Ponay\Ponay\Like\Enum\LikeableType;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class LikeTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;

    /**
     * @test
     */
    public function can_like_likable()
    {
        $liker = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($liker);

        $likeable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($likeable);

        $command = new LikeCommand($liker, $likeable, LikeableType::USER);
        $like = $this->sm->get('command.bus')->execute($command);

        $this->assertInstanceOf('Ponay\Ponay\Like\LikeEntity', $like, 'Like is not Like');
    }

    /**
     * @test
     */
    public function liking_post_will_increase_like_count()
    {
        $liker = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($liker);

        $likeable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($likeable);

        $command = new LikeCommand($liker, $likeable, LikeableType::USER);
        $this->sm->get('command.bus')->execute($command);

        $this->assertEquals(1, $likeable->getLikeCount());
    }

    /**
     * @test
     */
    public function relike_will_throw_exception()
    {
        $this->setExpectedException('Ponay\Ponay\Like\Command\Exception\AlreadyLikedException');

        $liker = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->sm->get('userrepo')->save($liker);

        $likeable = PostEntity::factory([]);
        $this->sm->get('postrepo')->save($likeable);

        $command = new LikeCommand($liker, $likeable, LikeableType::USER);
        $this->sm->get('command.bus')->execute($command);

        $command = new LikeCommand($liker, $likeable, LikeableType::USER);
        $this->sm->get('command.bus')->execute($command);
    }

    public function setUp()
    {
        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }
}