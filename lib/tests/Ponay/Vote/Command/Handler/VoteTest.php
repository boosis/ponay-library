<?php
namespace Ponay\Test\Ponay\Vote\Command\Handler;


use Ponay\Domain\Entity\Entity;
use Ponay\Domain\Repository\RepositoryLocator;
use Ponay\Ponay\App;
use Ponay\Ponay\Comment\CommentEntity;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;
use Ponay\Ponay\Vote\Command\Vote as VoteCommand;
use Ponay\Ponay\Vote\Enum\Direction;
use Ponay\Ponay\Vote\Enum\VotableType;
use Ponay\Ponay\Vote\VoteEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class VoteTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;
    /** @var  RepositoryLocator */
    protected $rm;

    /**
     * @test
     */
    public function can_vote_comment()
    {
        $votable = CommentEntity::factory([]);
        $this->rm->get($votable)->save($votable);

        $voter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($voter)->save($voter);

        $command = new VoteCommand($votable, $voter, Direction::UP, VotableType::COMMENT);
        $vote = $this->sm->get('command.bus')->execute($command);

        $this->assertInstanceOf(VoteEntity::class, $vote);
    }

    /**
     * @test
     */
    public function can_vote_post()
    {
        $votable = PostEntity::factory([]);
        $this->rm->get($votable)->save($votable);

        $voter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($voter)->save($voter);

        $command = new VoteCommand($votable, $voter, Direction::UP, VotableType::POST);
        $vote = $this->sm->get('command.bus')->execute($command);

        $this->assertInstanceOf(VoteEntity::class, $vote);
    }

    /**
     * @test
     */
    public function cannot_double_vote()
    {
        $this->setExpectedException('Ponay\Ponay\Vote\Command\Handler\Exception\DoubleVoteException');

        $votable = CommentEntity::factory([]);
        $this->rm->get($votable)->save($votable);

        $voter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($voter)->save($voter);

        $command = new VoteCommand($votable, $voter, Direction::UP, VotableType::COMMENT);
        $this->sm->get('command.bus')->execute($command);

        $command = new VoteCommand($votable, $voter, Direction::UP, VotableType::COMMENT);
        $this->sm->get('command.bus')->execute($command);
    }

    public function setUp()
    {
        $app = new App(APP_ENV);
        $app->boostrap();
        $this->sm = $app->getServiceManager();
        $this->rm = $this->sm->get('repomanager');
    }

    public function tearDown()
    {
        /** @var \MongoDb $mainDb */
        $mainDb = $this->sm->get('main.db');
        $mainDb->drop();
    }

    /**
     * @test
     */
    public function voting_increase_vote_count()
    {
        $votable = CommentEntity::factory([]);
        $this->rm->get($votable)->save($votable);

        $voter = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($voter)->save($voter);

        $voter2 = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($voter2)->save($voter2);

        $voter3 = UserEntity::factory(
            [
                'un' => 'username',
                'em' => 'email@example.com'
            ]);
        $this->rm->get($voter3)->save($voter3);

        $command = new VoteCommand($votable, $voter, Direction::UP, VotableType::COMMENT);
        $this->sm->get('command.bus')->execute($command);

        $command = new VoteCommand($votable, $voter2, Direction::DOWN, VotableType::COMMENT);
        $this->sm->get('command.bus')->execute($command);

        $command = new VoteCommand($votable, $voter3, Direction::DOWN, VotableType::COMMENT);
        $this->sm->get('command.bus')->execute($command);

        /** @var Entity $votable */
        $votable = $this->rm->get($votable)->getById($votable->getId());

        $this->assertEquals(1, $votable->getVotes()->getUpCount());
        $this->assertEquals(2, $votable->getVotes()->getDownCount());
        $this->assertEquals(-1, $votable->getVotes()->getTotal());
    }
}