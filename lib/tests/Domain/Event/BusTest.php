<?php
namespace Ponay\Test\Domain\Command\Event;

use Ponay\Domain\Event\BaseEvent;
use Ponay\Domain\Event\Event;
use Ponay\Domain\Event\EventDispatcher;

class BusTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function canAddListeners()
    {
        $bus = new EventDispatcher();
        $bus->addListener(
            'test_event',
            function (Event $event) {

            });

        $this->assertCount(1, $bus->getListeners('test_event'));
    }

    /**
     * @test
     */
    public function canTriggerAnEvent()
    {
        $bus = new EventDispatcher();
        $responses = $bus->dispatch('test_event', []);
        $this->assertInstanceOf('SplStack', $responses);
    }

    /**
     * @test
     */
    public function listenersWillBeCalledWhenEventIsTriggered()
    {
        $bus = new EventDispatcher();
        $bus->addListener(
            'test',
            function (Event $event) {
                return $event->getParam('id');
            });

        $params = ['id' => 'Some id'];
        /** @var \SplStack $responses */
        $responses = $bus->dispatch('test', $params);
        $this->assertCount(1, $responses);
        $this->assertEquals($responses->pop(), $params['id']);
    }

    /**
     * @test
     */
    public function starListenerWillListenEverything()
    {
        $bus = new EventDispatcher();
        $bus->addListener(
            '*',
            function (Event $event) {
                return $event->getParam('id');
            });
        $params = ['id' => 'Some id'];
        /** @var \SplStack $responses */
        $responses = $bus->dispatch('test', $params);
        $this->assertCount(1, $responses);
        $this->assertEquals($responses->pop(), $params['id']);
    }

    /**
     * @test
     */
    public function canListenEventObject()
    {
        $event = BaseEvent::factory('base', ['id' => 1]);
        $bus = new EventDispatcher();
        $bus->addListener(
            $event,
            function (Event $event) {
                return $event->getParam('id');
            });
        $params = ['id' => 'Some id'];
        /** @var \SplStack $responses */
        $responses = $bus->dispatch('base', $params);
        $this->assertCount(1, $responses);
        $this->assertEquals($responses->pop(), $params['id']);
    }

    /**
     * @test
     */
    public function canTriggerEventObject()
    {
        $params = ['id' => 'Some id'];
        $event = BaseEvent::factory('base', $params);
        $bus = new EventDispatcher();
        $bus->addListener(
            $event,
            function (Event $event) {
                return $event->getParam('id');
            });
        /** @var \SplStack $responses */
        $responses = $bus->dispatch($event);
        $this->assertCount(1, $responses);
        $this->assertEquals($responses->pop(), $params['id']);
    }

    /**
     * @test
     */
    public function willHonourPriority()
    {
        $bus = new EventDispatcher();
        $bus->addListener('test', function () {
            return 'Changed by 100';
        }, 100);
        $bus->addListener('test', function () {
            return 'Changed by 200';
        }, 200);
        /** @var \SplStack $responses */
        $responses = $bus->dispatch(BaseEvent::factory('test', []));
        $firstResponse = $responses->shift();
        $secondResponse = $responses->shift();
        $this->assertEquals('Changed by 200', $firstResponse);
        $this->assertEquals('Changed by 100', $secondResponse);
    }

} 