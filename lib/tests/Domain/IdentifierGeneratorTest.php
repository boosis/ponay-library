<?php
namespace vendor\ponay\ponay\lib\tests\Domain;

use Ponay\Domain\Entity\Identity\MongoIdIdentity;
use Ponay\Domain\Entity\IdentityGenerator;

class IdentifierGeneratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_create_from_native()
    {
        $native = new \MongoId();
        $generator = new IdentityGenerator(MongoIdIdentity::class);
        $mongoIdentifier = $generator->fromNative($native);

        $this->assertEquals($native, $mongoIdentifier->getValue());
    }

    /**
     * @test
     */
    public function can_create_from_string()
    {
        $mongoId = new \MongoId();
        $generator = new IdentityGenerator(MongoIdIdentity::class);
        $mongoIdentifier = $generator->fromString((string) $mongoId);
        $this->assertInstanceOf(MongoIdIdentity::class, $mongoIdentifier);
        $this->assertEquals($mongoId, $mongoIdentifier->getValue());
    }

    /**
     * @test
     */
    public function can_generate()
    {
        $generator = new IdentityGenerator(MongoIdIdentity::class);
        $mongoIdentifier = $generator->generate();
        $this->assertInstanceOf(MongoIdIdentity::class, $mongoIdentifier);
    }


}