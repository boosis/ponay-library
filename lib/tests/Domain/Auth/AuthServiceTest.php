<?php
namespace Ponay\Test\Domain\Auth;

use Ponay\Ponay\Auth\Adapter\Memory;
use Ponay\Ponay\Auth\AuthService;

class AuthServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function returns_false_if_no_identity()
    {
        $this->assertFalse(AuthService::factory(new Memory())->getIdentity());
    }

    public function setUp()
    {
        parent::setUp();
    }

}