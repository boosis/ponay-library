<?php
namespace Ponay\Test\Domain\Auth\Adapter;

use Ponay\Ponay\Auth\Adapter\Memory;
use Ponay\Ponay\Auth\Identity;

class MemoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Memory */
    protected $adapter;

    public function setUp()
    {
        parent::setUp();
        $this->adapter = new Memory();
    }

    /**
     * @test
     */
    public function will_return_array_if_identity()
    {
        $identity = $this->getMockBuilder(Identity::class)->disableOriginalConstructor()->setMethods(['getIdentityData', 'getRole'])->getMock();

        $this->adapter->setIdentity($identity);

        $identityData = $this->adapter->getIdentity();

        $this->assertEquals($identity, $identityData);
    }

    /**
     * @test
     */
    public function will_return_false_if_no_identity()
    {
        $this->assertFalse($this->adapter->hasIdentity());
    }


}