<?php
namespace Ponay\Test\Domain;

use Ponay\Domain\Cursor;

class CursorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_remove_array_item_from_cursor()
    {
        $elem1 = [
            'a1' => 'b1'
        ];

        $cursor = new Cursor();
        $cursor->add($elem1);
        $this->assertCount(1, $cursor);
        $cursor->remove($elem1);
        $this->assertCount(0, $cursor);
    }

    /**
     * @test
     */
    public function can_remove_stdClass()
    {
        $elem1 = new \stdClass();
        $elem1->prop = 'elem1';
        $elem2 = new \stdClass();
        $elem2->prop = 'elem2';

        $cursor = new Cursor();
        $cursor->add($elem1);
        $cursor->add($elem2);
        $this->assertCount(2, $cursor);
        $cursor->remove($elem1);
        $this->assertCount(1, $cursor);
        $this->assertEquals('elem2', $cursor->getNext()->prop);
    }

    /**
     * @test
     */
    public function can_remove_when_multiple_items()
    {
        $elem1 = [
            'a1' => 'b1'
        ];
        $elem2 = [
            'a1' => 'b1'
        ];

        $cursor = new Cursor();
        $cursor->add($elem1);
        $cursor->add($elem2);
        $this->assertCount(2, $cursor);
        $cursor->remove($elem1);
        $this->assertCount(1, $cursor);
    }

    public function setUp()
    {
        parent::setUp();
    }

}