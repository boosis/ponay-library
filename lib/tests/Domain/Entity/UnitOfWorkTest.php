<?php
namespace Ponay\Test\Domain\Entity;


use Ponay\Domain\Entity\UnitOfWork;
use Ponay\Ponay\User\UserEntity;

class UnitOfWorkTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_store_entity()
    {
        $uow = new UnitOfWork();
        $user = UserEntity::factory(
            [
                'un' => 'un1',
                'em' => '1@example.com'
            ]);
        $uow->set($user);

        $ret = $uow->retreive($user->getId(), get_class($user));
        $this->assertEquals(spl_object_hash($user), spl_object_hash($ret));
    }

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function stored_with_reference()
    {
        $uow = new UnitOfWork();
        $user = UserEntity::factory(
            [
                'un' => 'un1',
                'em' => '1@example.com'
            ]);
        $uow->set($user);

        $ret = $uow->retreive($user->getId(), get_class($user));
        $this->assertEquals(spl_object_hash($user), spl_object_hash($ret));
        $user->setProperty('em', '2@example.com');

        $ret = $uow->retreive($user->getId(), get_class($user));
        $this->assertEquals($user->getEmailAddress(), $ret->getEmailAddress());
    }

}