<?php
namespace Ponay\Test\Domain\Entity;

use Ponay\Domain\Dao\ArrayDao;
use Ponay\Domain\Db\Adapter\ArrayAdapter;
use Ponay\Domain\Entity\Manager;
use Ponay\Domain\Entity\UnitOfWork;
use Ponay\Domain\Repository\BaseRepository;
use Ponay\Domain\Repository\BaseRepositoryLocator;
use Ponay\Domain\ServiceLocator\BaseServiceLocator;
use Ponay\Ponay\User\UserEntity;
use Zend\ServiceManager\ServiceLocatorInterface;

class EntityManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Manager */
    protected $manager;
    /** @var  ServiceLocatorInterface */
    protected $sm;

    /**
     * @test
     */
    public function changing_uow_entity_will_change_entity()
    {
        $user = UserEntity::factory(
            [
                'un' => 'username1',
                'em' => '1@email.com'
            ]);
        $this->manager->persist($user);
        /** @var UnitOfWork $uow */
        $uow = $this->sm->get(UnitOfWork::class);
        /** @var UserEntity $ret */
        $ret = $uow->retreive($user);
        $ret->setProperty('un', 'username2');

        $this->assertEquals('username2', $user->getUserName());
    }

    /**
     * @test
     */
    public function double_flush_will_not_change()
    {
        $user = UserEntity::factory(
            [
                'un' => 'username1',
                'em' => '1@email.com'

            ]);
        $this->manager->persist($user);
        $this->manager->flush();
        $userId = $user->getId();
        $this->manager->flush();
        $this->assertEquals($userId, $user->getId());
    }

    /**
     * @test
     */
    public function flush_will_save_to_db()
    {
        $user = UserEntity::factory(
            [
                'un' => 'username1',
                'em' => '1@email.com'
            ]);
        $this->manager->persist($user);
        $this->manager->flush();
        $this->assertNotNull($user->getId());
    }

    /**
     * @test
     */
    public function flushing_pre_persisted_entity_will_update_the_entity()
    {
        $user = UserEntity::factory(
            [
                'un' => 'username1',
                'em' => '1@email.com'
            ]);
        $this->manager->persist($user);
        $this->manager->flush();
        $user->setProperty('em', '2@email.com');
        $userId = $user->getId();
        $this->manager->flush();

        /** @var UserEntity $retreived */
        $retreived = $this->manager->get($userId, UserEntity::class);
        $this->assertEquals('2@email.com', $retreived->getEmailAddress());
    }

    /**
     * @test
     */
    public function lets_see()
    {
        $this->assertInstanceOf(Manager::class, $this->manager);
    }

    public function setUp()
    {
        parent::setUp();
        $this->sm = new BaseServiceLocator();
        $this->sm->set(
            BaseRepository::class,
            function () {
                $service = BaseRepository::factory(ArrayDao::factory(new ArrayAdapter(), 'users', '_id'), UserEntity::class, '_id');

                return $service;
            });
        $this->sm->set(
            UnitOfWork::class,
            function () {
                return new UnitOfWork();
            });

        $repoManager = new BaseRepositoryLocator($this->sm);
        $repoManager->set(UserEntity::class, BaseRepository::class);

        $this->manager = new Manager($repoManager, $this->sm->get(UnitOfWork::class));
    }

    /**
     * @test
     */
    public function will_save_entity_to_uow()
    {
        $user = UserEntity::factory(
            [
                'un' => 'username1',
                'em' => '1@email.com'
            ]);
        $this->manager->persist($user);
        /** @var UnitOfWork $uow */
        $uow = $this->sm->get(UnitOfWork::class);
        $ret = $uow->retreive($user);
        $this->assertEquals(spl_object_hash($user), spl_object_hash($ret));
    }
}