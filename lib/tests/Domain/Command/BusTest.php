<?php

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\CommandBus;
use Ponay\Domain\ServiceLocator\BaseServiceLocator;
use Zend\ServiceManager\ServiceLocatorInterface;

class BusTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceLocatorInterface */
    protected $sm;
    /** @var  CommandBus */
    protected $bus;
    /** @var  Command */
    protected $command;

    public function setUp()
    {
        $this->sm = new BaseServiceLocator();
        $this->sm->set('event.bus', new \Ponay\Domain\Event\EventDispatcher());
        $this->bus = new CommandBus($this->sm, $this->sm->get('event.bus'));
    }

    private function getCommand()
    {
        if (!$this->command) {
            $this->command = $this->getMockBuilder('\Ponay\Domain\Command\Command')->disableOriginalConstructor()->setMethods(null)->getMock();
        }

        return $this->command;
    }

    /**
     * @test
     */
    public function canRegisterCommandHandlers()
    {
        $this->bus->registerHandler(get_class($this->getCommand()), function () {
            $response = new BaseResponse();
            $response->setPayload(1);

            return $response;

        });
        $result = $this->bus->execute($this->getCommand());
        $this->assertEquals(1, $result);
    }

    /**
     * @test
     */
    public function givenHandlerNotFound_willThrowException()
    {
        $this->setExpectedException('Ponay\Domain\Command\Exception\CommandHandlerNotFoundException');
        $this->bus->execute($this->getCommand());
    }

    /**
     * @test
     */
    public function givenInvalidHandler_willThrowException()
    {
        $this->setExpectedException('Ponay\Domain\Command\Exception\HandlerIsNotCallableException');
        $this->bus->registerHandler(get_class($this->getCommand()), 'invalid-callback');
        $this->bus->execute($this->getCommand());
    }


} 