<?php
namespace Ponay\Utility;

class String
{
    static public function generateSlug($string, $maxLength = 200)
    {
        $result = strtolower(trim($string));
        $result = preg_replace("/[^a-z0-9\s-_]/", "", $result);
        $result = trim(preg_replace("/[\s-_]+/", "-", $result), "-");
        $result = trim(substr($result, 0, $maxLength));
        $result = str_replace([' a ', ' an ', ' the '], '', $result);
        $result = !empty($result) ? $result : 'x';

        return $result;
    }

    static public function elipsis($text, $length = 200)
    {
        if (strlen($text) <= $length) {
            return $text;
        }
        return substr($text,0, $length-3) . '...';
    }
} 