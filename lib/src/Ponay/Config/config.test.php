<?php

return [
    'service_manager' => [
        'alias' => [
            'main.db'       => 'arraydb',
            'user.dao'      => 'userdaoarray',
            'post.dao'      => 'postdaoarray',
            'like.dao'      => 'likedaoarray',
            'comment.dao'   => 'commentdaoarray',
            'vote.dao'      => 'votedaoarray',
            'user.idgen'    => 'useridgenuuid',
            'post.idgen'    => 'postidgenuuid',
            'comment.idgen' => 'commentidgenuuid',
            'vote.idgen'    => 'voteidgenuuid',
            'like.idgen'    => 'likeidgenuuid',
        ],
    ],
];