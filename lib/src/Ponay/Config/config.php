<?php
use Ponay\Domain\Command\CommandBus;
use Ponay\Domain\Dao\ArrayDao;
use Ponay\Domain\Dao\MongoDao;
use Ponay\Domain\Db\Adapter\ArrayAdapter;
use Ponay\Domain\Entity\Manager;
use Ponay\Domain\Entity\UnitOfWork;
use Ponay\Domain\Event\EventDispatcher;
use Ponay\Domain\Repository\BaseRepository;
use Ponay\Domain\Repository\BaseRepositoryLocator;
use Ponay\Ponay\Auth\Adapter\Session;
use Ponay\Ponay\Auth\AuthService;
use Ponay\Ponay\Auth\Event\Listener\UserLoggedInWithEmail;
use Ponay\Ponay\Auth\Event\Listener\UserRegisteredWithEmail;
use Ponay\Ponay\Comment\Command\Handler\CreateComment;
use Ponay\Ponay\Comment\CommentEntity;
use Ponay\Ponay\Comment\CommentRepository;
use Ponay\Ponay\Comment\CommentView;
use Ponay\Ponay\Comment\Event\Listener\CommentableCommented;
use Ponay\Ponay\Like\Command\Handler\Like;
use Ponay\Ponay\Like\Command\Handler\Unlike;
use Ponay\Ponay\Like\Event\Listener\LikeableLiked;
use Ponay\Ponay\Like\Event\Listener\LikeableUnliked;
use Ponay\Ponay\Like\LikeEntity;
use Ponay\Ponay\Post\Command\Handler\CreatePost;
use Ponay\Ponay\Post\Command\Handler\DeletePost;
use Ponay\Ponay\Post\Command\Handler\EditPost;
use Ponay\Ponay\Post\Command\Handler\PublishPost;
use Ponay\Ponay\Post\Command\Handler\UnpublishPost;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\Post\PostId;
use Ponay\Ponay\Post\PostRepository;
use Ponay\Ponay\User\Command\Handler\BanUser;
use Ponay\Ponay\User\Command\Handler\EmailLogin;
use Ponay\Ponay\User\Command\Handler\EmailRegister;
use Ponay\Ponay\User\Password\Strategy\Sha1WithSalt;
use Ponay\Ponay\User\UserEntity;
use Ponay\Ponay\User\UserRepo;
use Ponay\Ponay\Vote\Command\Handler\Vote;
use Ponay\Ponay\Vote\Event\Listener\VotableVoted;
use Ponay\Ponay\Vote\VoteEntity;

return [
    'app'             => [
        'database' => [
            'connection' => [
                'server'         => 'mongodb://localhost:27017',
                'options'        => [
                    'connect' => true
                ],
                'driver_options' => []
            ],
            'name'       => 'ponay'
        ],
        'password' => [
            'options' => [
                'salt' => 'h4Unj%/hZ7D,?i7Kya%y9iGDd@AmxYm2p2v;>dykD8Xh>dVAB#'
            ],
        ],
        'facebook' => [
            'appId'     => 'xxxx',
            'appSecret' => 'yyyyy'
        ],
        'twitter'  => [],
        'google'   => [],
        'mail'     => [
            'services' => [
                'mandrill' => [
                    'smtp' => [
                        'host'     => 'smtp.mandrillapp.com',
                        'port'     => 587,
                        'username' => 'zapmeco@gmail.com',
                        'password' => 'NEWHs3JXZN35bKR7Jv2lQA',
                    ],
                    'api'  => [
                        'key' => 'z7ptfzTPZAUYb91hUxmMNA'
                    ]
                ]
            ]
        ],
    ],
    'service_manager' => [
        'alias'     => [
            'main.db'           => 'mongodb',
            'repo.manager'      => 'repomanager',
            'entity.manager'    => 'entitymanager',
            'event.bus'         => \Ponay\Domain\Event\EventDispatcher::class,
            'command.bus'       => \Ponay\Domain\Command\CommandBus::class,
            'auth.service'      => 'authservice',
            'password.strategy' => 'passwordstrategy',
            'user.dao'          => 'userdaomongo',
            'user.repo'         => 'userrepo',
            'post.dao'          => 'postdaomongo',
            'post.repo'         => 'postrepo',
            'like.dao'          => 'likedaomongo',
            'like.repo'         => 'likerepo',
            'comment.dao'       => 'commentdaomongo',
            'comment.repo'      => 'commentrepo',
            'comment.view'      => 'commentview',
            'vote.dao'          => 'votedaomongo',
            'vote.repo'         => 'voterepo',
        ],
        'factories' => [
            'mongodb'                                  => function ($sm) {
                $config = $sm->get('appconfig');
                $connection = new MongoClient($config['database']['connection']['server'], $config['database']['connection']['options'], $config['database']['connection']['driver_options']);

                $database = new MongoDB($connection, $config['database']['name']);

                return $database;
            },
            'arraydb'                                  => function () {
                $database = new ArrayAdapter();

                return $database;
            },
            'repomanager'                              => function ($sm) {
                $service = new BaseRepositoryLocator($sm);
                $service->set(UserEntity::class, 'userrepo');
                $service->set(PostEntity::class, 'postrepo');
                $service->set(CommentEntity::class, 'commentrepo');
                $service->set(LikeEntity::class, 'likerepo');
                $service->set(VoteEntity::class, 'voterepo');

                return $service;
            },
            'entitymanager'                            => function ($sm) {
                $service = new Manager($sm->get('repomanager'), new UnitOfWork());

                return $service;
            },
            \Ponay\Domain\Event\EventDispatcher::class => function ($sm) {
                $service = new EventDispatcher();
                $service->addListener(\Ponay\Ponay\Comment\Event\CommentableCommented::class, [new CommentableCommented($sm->get('repo.manager')), 'handle']);

                $service->addListener(\Ponay\Ponay\Like\Event\LikeableLiked::class, [new LikeableLiked($sm->get('repo.manager')), 'handle']);
                $service->addListener(\Ponay\Ponay\Like\Event\LikeableUnliked::class, [new LikeableUnliked($sm->get('repo.manager')), 'handle']);

                $service->addListener(\Ponay\Ponay\Vote\Event\VotableVoted::class, [new VotableVoted($sm->get('repo.manager')), 'handle']);

                $service->addListener(\Ponay\Ponay\User\Event\UserLoggedInWithEmail::class, [new UserLoggedInWithEmail($sm->get('auth.service')), 'handle']);
                $service->addListener(\Ponay\Ponay\User\Event\UserRegisteredWithEmail::class, [new UserRegisteredWithEmail($sm->get('auth.service')), 'handle']);

                $service->addListener(\Ponay\Ponay\Post\Event\PostReplied::class, [new \Ponay\Ponay\Post\Event\Listener\PostReplied($sm->get('repo.manager')), 'handle']);

                return $service;
            },
            \Ponay\Domain\Command\CommandBus::class    => function ($sm) {
                $service = new CommandBus($sm, $sm->get('event.bus'));

                return $service;
            },
            'userdaoarray'                             => function () {
                return ArrayDao::factory(new ArrayAdapter(), 'users', '_id');
            },
            'userdaomongo'                             => function ($sm) {
                return MongoDao::factory($sm->get('main.db'), 'users', '_id');
            },
            'userrepo'                                 => function ($sm) {
                $repo = UserRepo::factory($sm->get('user.dao'), UserEntity::class, '_id');

                return $repo;
            },
            'postdaomongo'                             => function ($sm) {
                return MongoDao::factory($sm->get('main.db'), 'posts', '_id', PostId::class);
            },
            'postdaoarray'                             => function () {
                return ArrayDao::factory(new ArrayAdapter(), 'posts', '_id');
            },
            'postrepo'                                 => function ($sm) {
                $repo = PostRepository::factory($sm->get('post.dao'), PostEntity::class, '_id');
                $repo->setUserRepo($sm->get('user.repo'));

                return $repo;
            },
            'likedaoarray'                             => function () {
                return ArrayDao::factory(new ArrayAdapter(), 'likes', '_id');
            },
            'likedaomongo'                             => function ($sm) {
                return MongoDao::factory($sm->get('main.db'), 'likes', '_id');
            },
            'likerepo'                                 => function ($sm) {
                $repo = BaseRepository::factory($sm->get('like.dao'), LikeEntity::class, '_id');

                return $repo;
            },
            'commentdaoarray'                          => function () {
                return ArrayDao::factory(new ArrayAdapter(), 'comments', '_id');
            },
            'commentdaomongo'                          => function ($sm) {
                return MongoDao::factory($sm->get('main.db'), 'comments', '_id');
            },
            'commentrepo'                              => function ($sm) {
                $repo = CommentRepository::factory($sm->get('comment.dao'), CommentEntity::class, '_id');
                $repo->setUserRepo($sm->get('user.repo'));

                return $repo;
            },
            'votedaoarray'                             => function () {
                return ArrayDao::factory(new ArrayAdapter(), 'votes', '_id');
            },
            'votedaomongo'                             => function ($sm) {
                return MongoDao::factory($sm->get('main.db'), 'votes', '_id');
            },
            'voterepo'                                 => function ($sm) {
                $repo = BaseRepository::factory($sm->get('vote.dao'), VoteEntity::class, '_id');

                return $repo;
            },
            CreateComment::class                       => function ($sm) {
                $service = new CreateComment($sm->get('comment.repo'));

                return $service;
            },
            Like::class                                => function ($sm) {
                $service = new Like($sm->get('like.repo'));

                return $service;
            },
            Unlike::class                              => function ($sm) {
                $service = new Unlike($sm->get('like.repo'));

                return $service;
            },
            CreatePost::class                          => function ($sm) {
                $service = new CreatePost($sm->get('post.repo'));

                return $service;
            },
            EditPost::class                            => function ($sm) {
                $service = new EditPost($sm->get('post.repo'), $sm->get('user.repo'));

                return $service;
            },
            PublishPost::class                         => function ($sm) {
                $service = new PublishPost($sm->get('post.repo'), $sm->get('user.repo'));

                return $service;
            },
            UnpublishPost::class                       => function ($sm) {
                $service = new UnpublishPost($sm->get('post.repo'), $sm->get('user.repo'));

                return $service;
            },
            DeletePost::class                          => function ($sm) {
                $service = new DeletePost($sm->get('post.repo'));

                return $service;
            },
            'passwordstrategy'                         => function ($sm) {
                $appConfig = $sm->get('appconfig');

                return new Sha1WithSalt($appConfig['password']['options']);
            },
            BanUser::class                             => function ($sm) {
                $service = new BanUser($sm->get('user.repo'));

                return $service;
            },
            EmailLogin::class                          => function ($sm) {
                $service = new EmailLogin($sm->get('user.repo'), $sm->get('password.strategy'));

                return $service;
            },
            EmailRegister::class                       => function ($sm) {
                $service = new EmailRegister($sm->get('user.repo'), $sm->get('password.strategy'));

                return $service;
            },
            Vote::class                                => function ($sm) {
                $service = new Vote($sm->get('vote.repo'));

                return $service;
            },
            'authservice'                              => function () {
                $service = new AuthService(new Session());

                return $service;
            },
            'commentview'                              => function ($sm) {
                $service = new CommentView($sm->get('comment.repo'));

                return $service;
            },
        ],
    ],
];