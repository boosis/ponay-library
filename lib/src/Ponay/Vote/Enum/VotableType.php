<?php
namespace Ponay\Ponay\Vote\Enum;

use Ponay\Domain\Enum;

class VotableType extends Enum
{
    const POST = 'post';
    const COMMENT = 'comment';
}