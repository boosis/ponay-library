<?php
namespace Ponay\Ponay\Vote\Enum;

use Ponay\Domain\Enum;

class Direction extends Enum
{
    const UP = 'up';
    const DOWN = 'down';
}