<?php
namespace Ponay\Ponay\Vote\Event\Listener;

use Ponay\Domain\Event\Event;
use Ponay\Domain\Repository\RepositoryLocator;
use Ponay\Ponay\Vote\Votable;
use Ponay\Ponay\Vote\VoteEntity;

class VotableVoted
{
    /** @var  RepositoryLocator */
    protected $repoManager;

    function __construct(RepositoryLocator $repoManager)
    {
        $this->repoManager = $repoManager;
    }

    public function handle(Event $event)
    {
        /** @var VoteEntity $vote */
        $vote = $event->getParam('vote');
        /** @var Votable $votable */
        $votable = $event->getParam('vtbl');
        if ($vote->isUpVote()) {
            $votable->upVote();
        } else {
            $votable->downVote();
        }
        $this->repoManager->get($votable)->save($votable);

        return true;
    }
}