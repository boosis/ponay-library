<?php
namespace Ponay\Ponay\Vote;

interface Votable
{
    public function getId();

    public function upVote();

    public function downVote();

    /**
     * @return Vote
     */
    public function getVotes();
}