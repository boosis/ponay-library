<?php
namespace Ponay\Ponay\Vote\Command;

use Ponay\Domain\Command\Command;
use Ponay\Ponay\Vote\Votable;
use Ponay\Ponay\Vote\Voter;

class Vote implements Command
{
    /** @var  Votable */
    public $votable;
    /** @var  Voter */
    public $voter;
    public $direction;
    public $votableType;

    public function __construct($votable, $voter, $direction, $votableType)
    {
        $this->votable = $votable;
        $this->voter = $voter;
        $this->direction = $direction;
        $this->votableType = $votableType;
    }


}