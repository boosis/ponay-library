<?php
namespace Ponay\Ponay\Vote\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\BaseRepository;
use Ponay\Ponay\Vote\Command\Handler\Exception\DoubleVoteException;
use Ponay\Ponay\Vote\Command\Vote as VoteCommand;
use Ponay\Ponay\Vote\Event\VotableVoted;
use Ponay\Ponay\Vote\VoteEntity;

/**
 * @method VoteCommand getCommand()
 */
class Vote extends AbstractHandler
{
    /**
     * @var BaseRepository
     */
    protected $voteRepository;

    function __construct(BaseRepository $voteRepository)
    {
        $this->voteRepository = $voteRepository;
    }


    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();

        if ($this->alreadyVoted()) {
            throw new DoubleVoteException();
        }

        $vote = VoteEntity::factory(
            [
                'vtbl' => $this->getCommand()->votable->getId(),
                'vtr'  => $this->getCommand()->voter->getId(),
                'd'    => $this->getCommand()->direction,
                'vt'   => $this->getCommand()->votableType
            ]);

        $this->voteRepository->save($vote);

        $response = new BaseResponse();
        $response->setPayload($vote);
        $response->triggerEvent(VotableVoted::factory(null, ['vote' => $vote, 'vtbl' => $this->getCommand()->votable, 'vtr' => $this->getCommand()->voter]));

        return $response;
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

    private function alreadyVoted()
    {
        $condition = [
            'vtbl' => $this->getCommand()->votable->getId(),
            'vtr'  => $this->getCommand()->voter->getId(),
            'vt'   => $this->getCommand()->votableType,
        ];

        return $this->voteRepository->count($condition) > 0;
    }
}