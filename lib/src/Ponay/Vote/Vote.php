<?php
namespace Ponay\Ponay\Vote;

class Vote
{
    protected $upCount = 0;
    protected $downCount = 0;
    protected $total = 0;

    public function __construct(array $voteData)
    {
        $this->upCount = $voteData['u'];
        $this->downCount = $voteData['d'];
        $this->total = $this->upCount - $this->downCount;
    }

    public function getUpCount()
    {
        return $this->upCount;
    }

    public function getDownCount()
    {
        return $this->downCount;
    }

    public function getTotal()
    {
        return $this->total;
    }

}