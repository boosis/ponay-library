<?php
namespace Ponay\Ponay\Vote;

use Ponay\Domain\Entity\AbstractEntity;
use Ponay\Ponay\Vote\Enum\Direction;

class VoteEntity extends AbstractEntity
{
    public function isUpVote()
    {
        return $this->getProperty('d') == Direction::UP;
    }

    function validateData($data)
    {
        // TODO: Implement validateData() method.
    }
}