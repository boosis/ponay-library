<?php
namespace Ponay\Ponay\User;

class UserName
{
    private $username;

    public function __construct($username)
    {
        $this->username = $username;
    }

    public function __toString()
    {
        return $this->username;
    }

    public function equals($compare)
    {
        return strcasecmp($this->username, $compare) === 0;
    }
}