<?php
namespace Ponay\Ponay\User;

use Ponay\Domain\Entity\AbstractEntity;
use Ponay\Domain\Entity\Exception\NotEmptyFieldMissingException;
use Ponay\Ponay\Auth\Enum\Role;
use Ponay\Ponay\Comment\Commenter;
use Ponay\Ponay\Like\Likeable;
use Ponay\Ponay\Like\Liker;
use Ponay\Ponay\Post\Poster;
use Ponay\Ponay\User\Enum\Status;
use Ponay\Ponay\Vote\Voter;

class UserEntity extends AbstractEntity implements Likeable, Liker, Commenter, Poster, Voter
{
    protected $notEmptyFields = ['un', 'em'];
    protected $defaults = [
        'r' => Role::CONTRIBUTER
    ];

    public function ban()
    {
        return $this->setProperty('sts', Status::BANNED);
    }

    public function decLikeCount()
    {
        $this->setProperty('lc', $this->getProperty('lc', 0) - 1);
    }

    public function getAvatarUrl()
    {
        return $this->getProperty('au');
    }

    public function getEmailAddress()
    {
        return $this->getProperty('em');
    }

    public function getLikeCount()
    {
        return $this->getProperty('lc');
    }

    public function getPassword()
    {
        return $this->getProperty('p', false);
    }

    public function getRole()
    {
        return $this->getProperty('r');
    }

    public function getSlug()
    {
        return new Slug($this->getUserName());
    }

    public function getUserName()
    {
        $value = $this->getProperty('un', false);
        if ($value) {
            return new UserName($value);
        }

        return false;
    }

    public function incLikeCount()
    {
        $this->setProperty('lc', $this->getProperty('lc', 0) + 1);
    }

    public function isAdmin()
    {
        return $this->getRole() == Role::ADMIN;
    }

    public function isBanned()
    {
        return $this->getProperty('sts') == Status::BANNED;
    }

    public function validateData($data)
    {
        foreach ($this->notEmptyFields as $fieldName) {
            if (empty($data[$fieldName])) {
                throw new NotEmptyFieldMissingException($fieldName);
            }
        }
    }
}