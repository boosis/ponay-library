<?php
namespace Ponay\Ponay\User;

use Ponay\Domain\Entity\Entity;
use Ponay\Domain\Repository\BaseRepository;
use Ponay\Domain\Repository\Exception\DublicateRecordException;

class UserRepo extends BaseRepository
{
    public function findByUserName($username, $fields = [])
    {
        $condition = [
            'un' => $username
        ];
        $result = $this->getDao()->fetchBy($condition, $fields, 1, 0);

        return $this->cursorToCollection($result)->first();
    }

    public function save(Entity $entity, $options = [])
    {
        if (!$entity->getId()) {
            if ($this->userExists($entity)) {
                throw new DublicateRecordException();
            }
        }

        parent::save($entity, $options);

        return $entity;
    }

    private function userExists(UserEntity $entity)
    {
        $condition = [
            '$or' => [
                ['un' => (string) $entity->getUserName()],
                ['em' => (string) $entity->getEmailAddress()]
            ]
        ];

        return $this->getDao()->count($condition) > 0;
    }
}