<?php
namespace Ponay\Ponay\User\Command;

use Ponay\Domain\Command\Command;
use Ponay\Ponay\User\UserEntity;

class BanUser implements Command
{
    /** @var  UserEntity */
    public $user;

    function __construct(UserEntity $user)
    {
        $this->user = $user;
    }
}
