<?php
namespace Ponay\Ponay\User\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\User\Command\EmailRegister as RegisterCommand;
use Ponay\Ponay\User\Event\UserRegisteredWithEmail;
use Ponay\Ponay\User\Exception\Register\EmailAlreadyRegisteredException;
use Ponay\Ponay\User\Exception\Register\PasswordTokenNotMatchedException;
use Ponay\Ponay\User\Exception\Register\UsernameTakenException;
use Ponay\Ponay\User\Password\Strategy\StrategyInterface;
use Ponay\Ponay\User\UserEntity;

/**
 * Class Register
 *
 * @method RegisterCommand getCommand()
 */
class EmailRegister extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $userRepository;
    /** @var  StrategyInterface */
    protected $passwordStrategy;

    public function __construct(Repository $userRepository, StrategyInterface $passwordStrategy)
    {
        $this->userRepository = $userRepository;
        $this->passwordStrategy = $passwordStrategy;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $user = UserEntity::factory(
            [
                'un' => $this->getCommand()->username,
                'em' => mb_strtolower($this->getCommand()->email),
                'p'  => $this->passwordStrategy->encrypt($this->getCommand()->password),
                'fn' => ucwords(mb_strtolower($this->getCommand()->firstname)),
                'ln' => ucwords(mb_strtolower($this->getCommand()->lastname)),
            ]
        );
        $this->userRepository->save($user);

        $response = new BaseResponse();
        $response->setPayload($user);
        $response->triggerEvent(UserRegisteredWithEmail::factory(null, ['user' => $user]));

        return $response;
    }

    public function validate()
    {
        if (!$this->getCommand()->username) {
            throw new InvalidCommandArgumentException('username');
        }
        if (!$this->getCommand()->password) {
            throw new InvalidCommandArgumentException('password');
        }
        if ($this->getCommand()->password !== $this->getCommand()->repassword) {
            throw new PasswordTokenNotMatchedException();
        }
        if (!$this->getCommand()->firstname) {
            throw new InvalidCommandArgumentException('firstname');
        }
        if (!$this->getCommand()->lastname) {
            throw new InvalidCommandArgumentException('lastname');
        }
        if (!$this->getCommand()->email) {
            throw new InvalidCommandArgumentException('email');
        }
        if ($this->userRepository->getByCondition(['un' => $this->getCommand()->username])->first()) {
            throw new UsernameTakenException();
        }
        if ($this->userRepository->getByCondition(['em' => $this->getCommand()->email])->first()) {
            throw new EmailAlreadyRegisteredException();
        }
    }
}
