<?php
namespace Ponay\Ponay\User\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\User\Command\BanUser as BanUserCommand;
use Ponay\Ponay\User\Event\UserBanned;
use Ponay\Ponay\User\UserEntity;

/**
 * Class BanUser
 *
 * @method BanUserCommand getCommand()
 */
class BanUser extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $userRepository;

    function __construct(Repository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function validate()
    {
        if (!$this->getCommand()->user instanceof UserEntity) {
            throw new InvalidCommandArgumentException('$user must me instance of UserEntity');
        }
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $user = $this->getCommand()->user;
        $user->ban();
        $this->userRepository->save($user);

        $response = new BaseResponse();
        $response->setPayload($user);
        $response->triggerEvent(UserBanned::factory(null, ['user' => $user]));

        return $response;
    }
}