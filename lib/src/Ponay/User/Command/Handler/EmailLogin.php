<?php
namespace Ponay\Ponay\User\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\User\Command\EmailLogin as EmailLoginCommand;
use Ponay\Ponay\User\Event\UserLoggedInWithEmail;
use Ponay\Ponay\User\Exception\Login\BannedUserException;
use Ponay\Ponay\User\Exception\Login\PasswordNotMatchedException;
use Ponay\Ponay\User\Exception\Login\UsernameNotFoundException;
use Ponay\Ponay\User\Password\Strategy\Dumb;
use Ponay\Ponay\User\Password\Strategy\StrategyInterface;
use Ponay\Ponay\User\UserEntity;

/**
 * Class EmailLogin
 *
 * @method EmailLoginCommand getCommand()
 */
class EmailLogin extends AbstractHandler
{
    /** @var Repository */
    protected $userRepository;
    /** @var  StrategyInterface */
    protected $passwordStrategy;

    public function __construct(Repository $userRepository, StrategyInterface $passwordStrategy = null)
    {
        $this->userRepository = $userRepository;
        $this->passwordStrategy = $passwordStrategy ? : new Dumb();
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $condition = [
            'un' => $this->getCommand()->username
        ];
        /** @var UserEntity $user */
        $user = $this->userRepository->getByCondition($condition)->first();
        if (!$user) {
            throw new UsernameNotFoundException('Username not found');
        }
        if ($user->getPassword() !== $this->passwordStrategy->encrypt($this->getCommand()->password)) {
            throw new PasswordNotMatchedException('Password did not match');
        }
        if ($user->isBanned()) {
            throw new BannedUserException('User banned');
        }

        $response = new BaseResponse();
        $response->setPayload($user);
        $response->triggerEvent(UserLoggedInWithEmail::factory(null, ['user' => $user]));

        return $response;
    }


    public function validate()
    {
        if (!$this->getCommand()->username) {
            throw new InvalidCommandArgumentException('username');
        }
        if (!$this->getCommand()->password) {
            throw new InvalidCommandArgumentException('password');
        }
    }
}
