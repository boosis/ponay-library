<?php
namespace Ponay\Ponay\User\Command;

use Ponay\Domain\Command\Command;

class EmailLogin implements Command
{
    public $username;
    public $password;

    public function __construct($username, $password)
    {
        $this->password = $password;
        $this->username = $username;
    }
}
