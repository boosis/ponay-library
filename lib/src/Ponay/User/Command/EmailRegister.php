<?php
namespace Ponay\Ponay\User\Command;

use Ponay\Domain\Command\Command;

class EmailRegister implements Command
{
    public $firstname;
    public $lastname;
    public $username;
    public $password;
    public $repassword;
    public $email;

    public function __construct($username, $firstname, $lastname, $password, $repassword, $email)
    {
        $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->password = $password;
        $this->repassword = $repassword;
        $this->username = $username;
    }

}
