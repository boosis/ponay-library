<?php
namespace Ponay\Ponay\User\Enum;

use Ponay\Domain\Enum;

class Status extends Enum
{
    const ACTIVE = '1';
    const BANNED = '-1';
} 