<?php
namespace Ponay\Ponay\User;

use Ponay\Utility\String;

class Slug
{
    private $slug;

    public function __construct($username)
    {
        $this->slug = String::generateSlug($username);
    }

    public function __toString()
    {
        return $this->slug;
    }

    public function equals($compare)
    {
        return strcasecmp($this->slug, $compare) === 0;
    }
}