<?php
namespace Ponay\Ponay\User\Password\Strategy;

class Dumb implements StrategyInterface
{
    public function __construct(array $options = null)
    {
    }

    public function encrypt($password)
    {
        return $password;
    }
}
