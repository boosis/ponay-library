<?php
namespace Ponay\Ponay\User\Password\Strategy;

use Ponay\Ponay\User\Exception\Password\Strategy\PasswordStrategyMissionOptionException;

class Sha1WithSalt implements StrategyInterface
{
    protected $salt;

    public function __construct(array $options = null)
    {
        if (!$options) {
            throw new PasswordStrategyMissionOptionException();
        }
        if (empty($options['salt'])) {
            throw new PasswordStrategyMissionOptionException();
        }
        $this->salt = $options['salt'];
    }

    public function encrypt($password)
    {
        return sha1($this->salt . $password);
    }
}
