<?php
namespace Ponay\Ponay\User\Password\Strategy;

interface StrategyInterface
{
    public function __construct(array $options = null);

    public function encrypt($password);
}
