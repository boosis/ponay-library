<?php
namespace Ponay\Ponay\User\Password\Strategy;

use Ponay\Ponay\User\Exception\Password\Strategy\PasswordStrategyMissionOptionException;

class Sha1WithDoubleSalt implements StrategyInterface
{
    protected $prefixSalt;
    protected $suffixSalt;

    public function __construct(array $options = null)
    {
        if (!$options) {
            throw new PasswordStrategyMissionOptionException();
        }
        if (empty($options['prefixsalt']) || empty($options['suffixsalt'])) {
            throw new PasswordStrategyMissionOptionException();
        }
        $this->prefixSalt = $options['prefixsalt'];
        $this->suffixSalt = $options['suffixsalt'];
    }

    public function encrypt($password)
    {
        return sha1($this->prefixSalt . $password . $this->suffixSalt);
    }
}
