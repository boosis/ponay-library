<?php
namespace Ponay\Ponay\User\Helper;

use Ponay\Ponay\User\UserEntity;

class AvatarSource
{
    static public function getAvatarUrl(UserEntity $userEntity, $size = 50)
    {
        if ($userEntity->getAvatarUrl()) {
            return $userEntity->getAvatarUrl();
        }
        $gravatarHash = md5(strtolower(trim($userEntity->getEmailAddress())));

        return 'http://www.gravatar.com/avatar/' . $gravatarHash . '/?s=' . $size;
    }
}