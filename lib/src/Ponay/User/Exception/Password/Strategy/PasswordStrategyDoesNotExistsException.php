<?php
namespace Ponay\Ponay\User\Exception\Password\Strategy;

class PasswordStrategyDoesNotExistsException extends \Exception
{
}
