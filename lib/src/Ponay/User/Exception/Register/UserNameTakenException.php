<?php
namespace Ponay\Ponay\User\Exception\Register;

class UsernameTakenException extends \Exception
{
    protected $message = 'User name already taken';
}
