<?php
namespace Ponay\Ponay\User\Exception\Register;

class EmailAlreadyRegisteredException extends \Exception
{
    protected $message = 'Email already registered';
}
