<?php
namespace Ponay\Ponay\User\Exception\Register;

class PasswordTokenNotMatchedException extends \Exception
{
    protected $message = 'Password and RePassword did not match';
}