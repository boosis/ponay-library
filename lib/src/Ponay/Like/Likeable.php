<?php
namespace Ponay\Ponay\Like;

interface Likeable
{
    public function getId();

    public function incLikeCount();

    public function decLikeCount();

    public function getLikeCount();
}