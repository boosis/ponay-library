<?php
namespace Ponay\Ponay\Like\Enum;

use Ponay\Domain\Enum;

class LikeableType extends Enum
{
    const USER = 'user';
    const POST = 'post';
    const COMMENT = 'comment';
}