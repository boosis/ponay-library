<?php
namespace Ponay\Ponay\Like\Event\Listener;

use Ponay\Domain\Event\Event;
use Ponay\Domain\Repository\RepositoryLocator;
use Ponay\Ponay\Like\Likeable;

class LikeableUnliked
{
    /** @var  RepositoryLocator */
    protected $repoManager;

    function __construct(RepositoryLocator $repoManager)
    {
        $this->repoManager = $repoManager;
    }

    public function handle(Event $event)
    {
        /** @var Likeable $likeable */
        $likeable = $event->getParam('lkbl');
        $likeable->decLikeCount();
        $this->repoManager->get($likeable)->save($likeable);

        return true;
    }
}