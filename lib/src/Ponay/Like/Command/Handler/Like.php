<?php
namespace Ponay\Ponay\Like\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Like\Command\Exception\AlreadyLikedException;
use Ponay\Ponay\Like\Command\Like as LikeCommand;
use Ponay\Ponay\Like\Event\LikeableLiked;
use Ponay\Ponay\Like\LikeEntity;

/**
 * Class Like
 *
 * @method LikeCommand getCommand()
 */
class Like extends AbstractHandler
{
    /** @var  Repository */
    protected $likeRepository;

    public function __construct(Repository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $like = LikeEntity::factory(
            [
                'lkbl' => $this->getCommand()->likeable->getId(),
                'lkr'  => $this->getCommand()->liker->getId(),
                't'    => $this->getCommand()->type
            ]);
        $this->likeRepository->save($like);
        $response = new BaseResponse();
        $response->setPayload($like);
        $response->triggerEvent(LikeableLiked::factory(null, ['like' => $like, 'lkbl' => $this->getCommand()->likeable, 'lkr' => $this->getCommand()->liker, 'type' => $this->getCommand()->type]));

        return $response;
    }

    public function validate()
    {
        if ($this->checkIfAlreadyLiked()) {
            throw new AlreadyLikedException();
        }
    }

    private function checkIfAlreadyLiked()
    {
        $likeable = $this->getCommand()->likeable;
        $liker = $this->getCommand()->liker;
        $condition = ['lkbl' => $likeable->getId(), 'lkr' => $liker->getId(), 't' => $this->getCommand()->type];

        return $this->likeRepository->count($condition) > 0;
    }
}