<?php
namespace Ponay\Ponay\Like\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Like\Command\Exception\NotLikedException;
use Ponay\Ponay\Like\Command\Unlike as UnlikeCommand;
use Ponay\Ponay\Like\Event\LikeableUnliked;

/**
 * Class Unlike
 *
 * @method UnlikeCommand getCommand()
 */
class Unlike extends AbstractHandler
{
    /** @var  Repository */
    protected $likeRepository;

    public function __construct(Repository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $condition = ['lkbl' => $this->getCommand()->likeable->getId(), 'lkr' => $this->getCommand()->liker->getId(), 't' => $this->getCommand()->type];
        $this->likeRepository->delete($condition);
        $response = new BaseResponse();
        $response->setPayload(true);
        $response->triggerEvent(LikeableUnliked::factory(null, ['lkbl' => $this->getCommand()->likeable, 'lkr' => $this->getCommand()->liker, 'type' => $this->getCommand()->type]));

        return $response;
    }

    public function validate()
    {
        if (!$this->checkIfAlreadyLiked()) {
            throw new NotLikedException();
        }
    }

    private function checkIfAlreadyLiked()
    {
        $likeable = $this->getCommand()->likeable;
        $liker = $this->getCommand()->liker;
        $condition = ['lkbl' => $likeable->getId(), 'lkr' => $liker->getId(), 't' => $this->getCommand()->type];

        return $this->likeRepository->count($condition) > 0;
    }
}