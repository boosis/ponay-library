<?php
namespace Ponay\Ponay\Like\Command;

use Ponay\Domain\Command\Command;
use Ponay\Ponay\Like\Likeable;
use Ponay\Ponay\Like\Liker;

class Unlike implements Command
{
    /** @var  Liker */
    public $liker;
    /** @var  Likeable */
    public $likeable;
    public $type;

    function __construct(Liker $liker, Likeable $likeable, $type)
    {
        $this->liker = $liker;
        $this->likeable = $likeable;
        $this->type = $type;
    }


} 