<?php
namespace Ponay\Ponay\Auth\Adapter;

use Ponay\Ponay\Auth\Identity;

class Memory implements Adapter
{
    protected $namespace;
    protected $storage;

    public function __construct($namespace = '__APP_AUTH__')
    {
        $this->namespace = $namespace;
        $this->storage = [];
    }

    public function clearIdentity()
    {
        unset($this->storage[$this->namespace]);
    }

    public function getIdentity()
    {
        if ($this->hasIdentity()) {
            return $this->storage[$this->namespace];
        }

        return null;
    }

    public function hasIdentity()
    {
        if (empty($this->storage[$this->namespace])) {
            return false;
        }

        return true;
    }

    public function setIdentity(Identity $identity)
    {
        $this->storage[$this->namespace] = $identity;
    }
}