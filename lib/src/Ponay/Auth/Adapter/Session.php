<?php
namespace Ponay\Ponay\Auth\Adapter;

use Carbon\Carbon;
use Ponay\Ponay\Auth\Identity;

class Session implements Adapter
{
    protected $namespace;
    protected $storage;

    public function __construct($namespace = '__APP_AUTH__')
    {
        $this->namespace = $namespace;
        $this->initStorage();
    }

    private function initStorage()
    {
        if (php_sapi_name() === 'cli') {
            $this->storage = [];
        } else {
            if (!$this->isSessionStarted()) {
                session_start();
            }
            $cookieName = session_name();
            if (empty($_COOKIE[$cookieName])) {
                $_COOKIE[$cookieName] = session_id();
            }
            $expires = Carbon::now()->addYear()->timestamp;
            $lifeTime = ($expires - Carbon::now()->timestamp);
            session_set_cookie_params($lifeTime, '/', null, false, true);
            $cookieParams = session_get_cookie_params();
            setcookie($cookieName, $_COOKIE[$cookieName], $expires, $cookieParams['path'], $cookieParams['domain'], $cookieParams['secure'], $cookieParams['httponly']);
            $this->storage = &$_SESSION;
        }
    }

    public function clearIdentity()
    {
        unset($this->storage[$this->namespace]);
    }

    public function getIdentity()
    {
        if ($this->hasIdentity()) {
            return unserialize($this->storage[$this->namespace]);
        }

        return null;
    }

    public function hasIdentity()
    {
        if (empty($this->storage[$this->namespace])) {
            return false;
        }

        return true;
    }

    public function setIdentity(Identity $identity)
    {
        $this->storage[$this->namespace] = serialize($identity);
    }

    private function isSessionStarted()
    {
        if (php_sapi_name() !== 'cli') {
            if (version_compare(phpversion(), '5.4.0', '>=')) {
                return session_status() === PHP_SESSION_ACTIVE ? true : false;
            } else {
                return session_id() === '' ? false : true;
            }
        }

        return false;
    }


}