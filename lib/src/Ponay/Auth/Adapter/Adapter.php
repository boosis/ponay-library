<?php
namespace Ponay\Ponay\Auth\Adapter;

use Ponay\Ponay\Auth\Identity;

interface Adapter
{
    public function hasIdentity();

    public function getIdentity();

    public function setIdentity(Identity $identity);

    public function clearIdentity();
}