<?php
namespace Ponay\Ponay\Auth;

interface Identity
{
    public function getId();

    public function getRole();
}