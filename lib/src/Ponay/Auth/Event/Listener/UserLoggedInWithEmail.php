<?php
namespace Ponay\Ponay\Auth\Event\Listener;

use Ponay\Domain\Event\Event;
use Ponay\Ponay\Auth\AuthService;

class UserLoggedInWithEmail
{
    /** @var  AuthService */
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }


    public function handle(Event $event)
    {
        $user = $event->getParam('user');

        $this->authService->setIdentity($user);
    }
}