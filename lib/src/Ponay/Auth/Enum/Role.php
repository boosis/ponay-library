<?php
namespace Ponay\Ponay\Auth\Enum;

use Ponay\Domain\Enum;

class Role extends Enum
{
    const VISITOR = 'visitor';
    const CONTRIBUTER = 'contributer';
    const EDITOR = 'editor';
    const ADMIN = 'admin';
}