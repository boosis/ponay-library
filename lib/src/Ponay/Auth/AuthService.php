<?php
namespace Ponay\Ponay\Auth;

use Ponay\Ponay\Auth\Adapter\Adapter;
use Ponay\Ponay\Auth\Adapter\Memory;

class AuthService
{
    /** @var  Adapter */
    protected $adapter;

    public function __construct(Adapter $adapter = null)
    {
        if (!$adapter) {
            $adapter = new Memory();
        }
        $this->adapter = $adapter;
    }

    public function clearIdentity()
    {
        $this->adapter->clearIdentity();
    }

    static public function factory(Adapter $adapter = null)
    {
        $self = new static($adapter);

        return $self;
    }

    public function getIdentity()
    {
        return $this->adapter->getIdentity();
    }

    public function hasIdentity()
    {
        return $this->adapter->hasIdentity();
    }

    public function setIdentity(Identity $identity)
    {
        $this->adapter->setIdentity($identity);
    }
}