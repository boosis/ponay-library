<?php
namespace Ponay\Ponay;

use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\ArrayUtils;

class App
{
    /** @var  ServiceManager */
    protected $serviceManager;
    protected $env;

    public function __construct($env)
    {
        $this->env = $env;
    }

    public function boostrap()
    {
        $baseDir = dirname(__DIR__);
        $config = $this->buildConfig([], $baseDir . '/Ponay/Config', $this->env);
        $this->setupServiceManager($config);
    }

    private function setupServiceManager($config)
    {
        $appConfig = $config['app'];
        $smConfig = $config['service_manager'];

        $sm = new ServiceManager();
        $sm->setFactory('appconfig', function () use ($appConfig) {
            return $appConfig;
        });

        foreach ($smConfig['alias'] as $alias => $name) {
            $sm->setAlias($alias, $name);
        }
        foreach ($smConfig['factories'] as $name => $factory) {
            $sm->setFactory($name, $factory);
        }
        $this->serviceManager = $sm;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    private function buildConfig($params = [], $configDir = null, $env)
    {
        $files = [];
        if ($configDir) {
            $files = [
                rtrim($configDir, '/') . '/config.php',
                rtrim($configDir, '/') . '/config.' . $env . '.php'
            ];
        }
        $config = $params;
        if (!empty($files)) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    $conf = include $file;
                    $config = ArrayUtils::merge($config, $conf);
                }
            }
        }

        return $config;
    }

    public function get($serviceName)
    {
        return $this->getServiceManager()->get($serviceName);
    }
}