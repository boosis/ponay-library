<?php
namespace Ponay\Ponay\Post;

use Ponay\Utility\String;

class Slug
{
    private $slug;

    public function __construct($title, $length = 200)
    {
        $this->slug = String::generateSlug($title, $length);
    }

    public function __toString()
    {
        return $this->slug;
    }

    public function equals($compare)
    {
        return strcasecmp($this->slug, $compare) === 0;
    }
}