<?php
namespace Ponay\Ponay\Post\Enum;

use Ponay\Domain\Enum;

class Status extends Enum
{
    const DELETED = 'deleted';
    const DRAFT = 'draft';
    const PUBLISHED = 'published';
    const ALL = 'all';
}