<?php
namespace Ponay\Ponay\Post;

use Carbon\Carbon;
use Ponay\Domain\Entity\AbstractEntity;
use Ponay\Ponay\Auth\Enum\Role;
use Ponay\Ponay\Auth\Identity;
use Ponay\Ponay\Comment\Commentable;
use Ponay\Ponay\Like\Likeable;
use Ponay\Ponay\Post\Enum\Status;
use Ponay\Ponay\Post\Helper\UrlHelper;
use Ponay\Ponay\User\UserEntity;
use Ponay\Ponay\Vote\Votable;
use Ponay\Ponay\Vote\Vote;
use Ponay\Utility\DateTime;

class PostEntity extends AbstractEntity implements Likeable, Commentable, Votable
{
    protected $defaults = [
        'st' => Status::DRAFT,
    ];

    public function canDelete(Identity $identity = null)
    {
        if (!$identity) {
            return false;
        }
        if ($identity->getRole() == Role::ADMIN || $identity->getRole() == Role::EDITOR) {
            return true;
        }

        if ($this->getPosterId() == $identity->getId()) {
            return true;
        }

        return false;
    }

    public function canEdit(Identity $identity = null)
    {
        if (!$identity) {
            return false;
        }
        if ($identity->getRole() == Role::ADMIN || $identity->getRole() == Role::EDITOR) {
            return true;
        }

        if ($this->getPosterId() == $identity->getId()) {
            return true;
        }

        return false;
    }

    public function canPreview(Identity $identity = null)
    {
        if (!$identity) {
            return false;
        }

        if ($identity->getRole() == Role::ADMIN || $identity->getRole() == Role::EDITOR) {
            return true;
        }

        if ($this->getPosterId() == $identity->getId()) {
            return true;
        }

        return false;
    }

    public function canPublish(Identity $identity = null)
    {
        if (!$identity) {
            return false;
        }

        if ($identity->getRole() == Role::ADMIN || $identity->getRole() == Role::EDITOR) {
            return true;
        }

        if ($this->getPosterId() == $identity->getId()) {
            return true;
        }

        return false;
    }

    public function canUnpublish(Identity $identity = null)
    {
        if (!$identity) {
            return false;
        }
        if ($identity->getRole() == Role::ADMIN || $identity->getRole() == Role::EDITOR) {
            return true;
        }

        if ($this->getPosterId() == $identity->getId()) {
            return true;
        }

        return false;
    }

    public function decLikeCount()
    {
        $this->setProperty('lc', $this->getProperty('lc', 0) - 1);
    }

    public function delete(UserEntity $deletedBy)
    {
        $this->setProperty('dby', $deletedBy->getId());
        $this->setProperty('st', Status::DELETED);
    }

    public function deletedBy()
    {
        return $this->getProperty('dby', $this->getPosterId());
    }

    public function downVote()
    {
        $votes = $this->getVoteCounts();
        $votes['d']++;
        $this->setProperty('v', $votes);
    }

    public function draft()
    {
        $this->setProperty('st', Status::DRAFT);
        $this->setProperty('pt', null);
    }

    public function getCommentCount()
    {
        return $this->getProperty('cc', 0);
    }

    public function getDescription()
    {
        return $this->getProperty('d');
    }

    public function getLikeCount()
    {
        return $this->getProperty('lc');
    }

    public function getPostUrl()
    {
        return UrlHelper::getPostUrl($this);
    }

    /**
     * @return Poster
     */
    public function getPoster()
    {
        return $this->getTempProperty('poster');
    }

    public function getPosterId()
    {
        return $this->getProperty('pid');
    }

    public function getRelativeCreationTime()
    {
        $creationTime = $this->getProperty('crt');

        return DateTime::FormatTime($creationTime);
    }

    public function getRelativePublishTime()
    {
        $publishTime = $this->getProperty('pt');
        if (!$publishTime) {
            return 'N/A';
        }

        return DateTime::FormatTime($publishTime);
    }

    public function getRepliesUrl()
    {
        return UrlHelper::getRepliesUrl($this);
    }

    public function getReplyCount()
    {
        return $this->getProperty('rc', 0);
    }

    public function getReplyUrl()
    {
        return UrlHelper::getReplyUrl($this);
    }

    public function getShortDescription()
    {
        return $this->getProperty('sd');
    }

    public function getSlug()
    {
        return new Slug($this->getTitle(), 100);
    }

    public function getStatus()
    {
        return $this->getProperty('st');
    }

    public function getTags($first = null, $count = null)
    {
        $returnTags = [];
        $tags = $this->getProperty('ts');
        foreach ($tags as $tag) {
            if ($first && $tag == $first) {
                array_unshift($returnTags, $tag);
            } else {
                $returnTags[] = $tag;
            }
        }
        if ($count) {
            return array_slice($returnTags, 0, $count);
        }

        return $returnTags;
    }

    public function getTitle()
    {
        return $this->getProperty('t');
    }

    public function getVotes()
    {
        return new Vote($this->getVoteCounts());
    }

    public function incLikeCount()
    {
        $this->setProperty('lc', $this->getProperty('lc', 0) + 1);
    }

    public function newReplyAdded($time = null)
    {
        $this->setProperty('lron', $time && is_int($time) ? $time : Carbon::now()->timestamp);
        $this->setProperty('rc', $this->getReplyCount() + 1);
    }

    public function isAReply()
    {
        return $this->replyToId() ? true : false;
    }

    public function isCommentingEnabled()
    {
        return $this->isPublished();
    }

    public function isDeleted()
    {
        return $this->getProperty('st', Status::DRAFT) == Status::DELETED;
    }

    public function isDraft()
    {
        return $this->getProperty('st', Status::DRAFT) == Status::DRAFT;
    }

    public function isPublished()
    {
        return $this->getProperty('st', Status::DRAFT) == Status::PUBLISHED;
    }

    public function newCommentAdded($time = null)
    {
        $this->setProperty('cc', $this->getCommentCount() + 1);
        $this->setProperty('lcon', $time && is_int($time) ? $time : Carbon::now()->timestamp);
    }

    public function publish()
    {
        $this->setProperty('pt', time());
        $this->setProperty('st', Status::PUBLISHED);
    }

    public function replyToId()
    {
        return $this->getProperty('rt');
    }

    public function upVote()
    {
        $votes = $this->getVoteCounts();
        $votes['u']++;
        $this->setProperty('v', $votes);
    }

    public function validateData($data)
    {
        // TODO: Implement validateData() method.
    }

    private function getVoteCounts()
    {
        return $this->getProperty('v', ['u' => 0, 'd' => 0]);
    }
}