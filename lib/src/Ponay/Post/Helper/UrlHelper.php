<?php
namespace Ponay\Ponay\Post\Helper;

use Ponay\Ponay\Post\PostEntity;

class UrlHelper
{
    static public function getPostUrl(PostEntity $postEntity)
    {
        return $postEntity->getPoster()->getSlug() . '/' . $postEntity->getSlug() . '/' . (string) $postEntity->getId();
    }

    public static function getRepliesUrl(PostEntity $postEntity)
    {
        return $postEntity->getPoster()->getSlug() . '/' . $postEntity->getSlug() . '/' . (string) $postEntity->getId() . '/replies';
    }

    public static function getReplyUrl(PostEntity $postEntity)
    {
        return 'post/' . (string) $postEntity->getId() . '/reply';
    }
}