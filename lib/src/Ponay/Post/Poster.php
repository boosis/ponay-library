<?php
namespace Ponay\Ponay\Post;

use Ponay\Ponay\Auth\Identity;

interface Poster extends Identity
{
    public function getId();

    public function getSlug();
}