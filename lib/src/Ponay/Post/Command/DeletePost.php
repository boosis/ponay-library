<?php
namespace Ponay\Ponay\Post\Command;

use Ponay\Domain\Command\Command;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;

class DeletePost implements Command
{
    public $postEntity;
    public $user;

    function __construct(PostEntity $postEntity, UserEntity $user)
    {
        $this->postEntity = $postEntity;
        $this->user = $user;
    }


}