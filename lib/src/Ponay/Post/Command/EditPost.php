<?php
namespace Ponay\Ponay\Post\Command;

use Ponay\Domain\Command\Command;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;

class EditPost implements Command
{
    public $postEntity;
    public $title;
    public $shortDescription;
    public $description;
    public $tags;
    public $user;

    function __construct(PostEntity $postEntity, UserEntity $user, $title, $shortDescription, $description, array $tags = [])
    {
        $this->postEntity = $postEntity;
        $this->title = $title;
        $this->shortDescription = $shortDescription;
        $this->description = $description;
        $this->tags = $tags;
        $this->user = $user;
    }


}