<?php
namespace Ponay\Ponay\Post\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Exception\NotAuthorizedException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Post\Command\PublishPost as PublishPostCommand;
use Ponay\Ponay\Post\Event\PostPublished;
use Ponay\Ponay\Post\Exception\DeletedByAdminException;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;

/**
 * Class PublishPost
 *
 * @method PublishPostCommand getCommand()
 */
class PublishPost extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $postRepository;
    /**
     * @var Repository
     */
    protected $userRepository;

    function __construct(Repository $postRepository, Repository $userRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $entity = $this->getCommand()->postEntity;
        $entity->publish();
        $this->postRepository->save($entity);

        $response = new BaseResponse();
        $response->setPayload($entity);
        $response->triggerEvent(PostPublished::factory(null, ['post' => $entity]));

        return $response;
    }

    public function validate()
    {
        if (!$this->getCommand()->postEntity instanceof PostEntity) {
            throw new InvalidCommandArgumentException('postEntity');
        }

        if ($this->getCommand()->postEntity->isDeleted()) {
            /** @var UserEntity $deletedBy */
            $deletedBy = $this->userRepository->getById($this->getCommand()->postEntity->deletedBy());
            if ($deletedBy->getId() != $this->getCommand()->user->getId() && !$this->getCommand()->user->isAdmin()) {
                throw new DeletedByAdminException();
            }
        }

        if (!$this->getCommand()->postEntity->canPublish($this->getCommand()->user)) {
            throw new NotAuthorizedException();
        }
    }
}