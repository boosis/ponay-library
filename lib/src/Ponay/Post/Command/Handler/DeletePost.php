<?php
namespace Ponay\Ponay\Post\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Exception\NotAuthorizedException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Post\Command\DeletePost as DeletePostCommand;
use Ponay\Ponay\Post\Event\PostDeleted;
use Ponay\Ponay\Post\Exception\AlreadyDeletedException;
use Ponay\Ponay\Post\PostEntity;

/**
 * Class DeletePost
 *
 * @method DeletePostCommand getCommand()
 */
class DeletePost extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $postRepository;

    function __construct(Repository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $entity = $this->getCommand()->postEntity;
        $entity->delete($this->getCommand()->user);
        $this->postRepository->save($entity);

        $response = new BaseResponse();
        $response->setPayload($entity);
        $response->triggerEvent(PostDeleted::factory(null, ['post' => $entity]));

        return $response;
    }

    public function validate()
    {
        if (!$this->getCommand()->postEntity instanceof PostEntity) {
            throw new InvalidCommandArgumentException('postEntity');
        }

        if ($this->getCommand()->postEntity->isDeleted()) {
            throw new AlreadyDeletedException();
        }

        if (!$this->getCommand()->postEntity->canDelete($this->getCommand()->user)) {
            throw new NotAuthorizedException();
        }
    }
}