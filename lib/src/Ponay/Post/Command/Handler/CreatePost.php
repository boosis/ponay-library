<?php
namespace Ponay\Ponay\Post\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Exception\NotAuthorizedException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Auth\Enum\Role;
use Ponay\Ponay\Post\Command\CreatePost as CreatePostCommand;
use Ponay\Ponay\Post\Enum\Status;
use Ponay\Ponay\Post\Event\PostCreated;
use Ponay\Ponay\Post\Event\PostReplied;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\Post\Poster;

/**
 * Class CreatePost
 *
 * @method CreatePostCommand getCommand()
 */
class CreatePost extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $postRepository;

    function __construct(Repository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $tags = $this->getCommand()->tags;
        $tags = array_map('trim', $tags);
        $tags = array_filter(
            $tags,
            function ($elem) {
                return $elem;
            });
        $postData = [
            'pid' => $this->getCommand()->author->getId(),
            't'   => $this->getCommand()->title,
            'sd'  => $this->getCommand()->shortDescription,
            'd'   => $this->getCommand()->description,
            'ts'  => $tags,
            'st'  => $this->getCommand()->publish ? Status::PUBLISHED : Status::DRAFT
        ];
        if ($this->getCommand()->publish) {
            $postData['pt'] = time();
        }
        $isReply = false;
        if ($this->getCommand()->replyTo) {
            $isReply = true;
            $postData['rt'] = $this->getCommand()->replyTo->getId();
        }
        $entity = PostEntity::factory($postData);
        $entity->setTempProperty('poster', $this->getCommand()->author);
        $this->postRepository->save($entity);

        $response = new BaseResponse();
        $response->setPayload($entity);
        $response->triggerEvent(PostCreated::factory(null, ['post' => $entity]));
        if ($isReply) {
            $response->triggerEvent(PostReplied::factory(null, ['post' => $entity, 'replyTo' => $this->postRepository->getById($entity->replyToId())]));
        }

        return $response;
    }

    public function validate()
    {
        if (!$this->getCommand()->author instanceof Poster) {
            throw new InvalidCommandArgumentException('author');
        }

        if (!$this->getCommand()->author->getRole() == Role::ADMIN && !$this->getCommand()->author->getRole() == Role::CONTRIBUTER && !$this->getCommand()->author->getRole() == Role::EDITOR) {
            throw new NotAuthorizedException();
        }

        if (!$this->getCommand()->title) {
            throw new InvalidCommandArgumentException('title');
        }

        if (!$this->getCommand()->description) {
            throw new InvalidCommandArgumentException('description');
        }
    }
}