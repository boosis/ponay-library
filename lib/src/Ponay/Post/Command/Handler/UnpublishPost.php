<?php
namespace Ponay\Ponay\Post\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Exception\NotAuthorizedException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Post\Command\UnpublishPost as UnpublishPostCommand;
use Ponay\Ponay\Post\Event\PostUnpublished;
use Ponay\Ponay\Post\Exception\DeletedByAdminException;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;

/**
 * Class UnpublishPost
 *
 * @method UnpublishPostCommand getCommand()
 */
class UnpublishPost extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $postRepository;
    /**
     * @var Repository
     */
    protected $userRepository;

    function __construct(Repository $postRepository, Repository $userRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $entity = $this->getCommand()->postEntity;
        $entity->draft();
        $this->postRepository->save($entity);

        $response = new BaseResponse();
        $response->setPayload($entity);
        $response->triggerEvent(PostUnpublished::factory(null, ['post' => $entity]));

        return $response;
    }

    public function validate()
    {
        if (!$this->getCommand()->postEntity instanceof PostEntity) {
            throw new InvalidCommandArgumentException('postEntity');
        }

        if ($this->getCommand()->postEntity->isDeleted()) {
            /** @var UserEntity $deletedBy */
            $deletedBy = $this->userRepository->getById($this->getCommand()->postEntity->deletedBy());
            if ($deletedBy->getId() != $this->getCommand()->user->getId() && !$this->getCommand()->user->isAdmin()) {
                throw new DeletedByAdminException();
            }
        }

        if (!$this->getCommand()->postEntity->canUnpublish($this->getCommand()->user)) {
            throw new NotAuthorizedException();
        }
    }
}