<?php
namespace Ponay\Ponay\Post\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Exception\InvalidCommandArgumentException;
use Ponay\Domain\Command\Exception\NotAuthorizedException;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Post\Command\EditPost as EditPostCommand;
use Ponay\Ponay\Post\Event\PostEdited;
use Ponay\Ponay\Post\Exception\DeletedByAdminException;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\User\UserEntity;

/**
 * Class EditPost
 *
 * @method EditPostCommand getCommand()
 */
class EditPost extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $postRepository;
    /**
     * @var Repository
     */
    protected $userRepository;

    function __construct(Repository $postRepository, Repository $userRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $original = $this->getCommand()->postEntity;
        $entity = clone $original;

        $changes = [];
        if ($original->getTitle() !== $this->getCommand()->title) {
            $changes['t'] = $this->getCommand()->title;
        }
        if ($original->getShortDescription() !== $this->getCommand()->shortDescription) {
            $changes['sd'] = $this->getCommand()->shortDescription;
        }
        if ($original->getDescription() !== $this->getCommand()->description) {
            $changes['d'] = $this->getCommand()->description;
        }
        $tags = [];
        foreach ($this->getCommand()->tags as $tag) {
            $tag = trim($tag);
            if ($tag) {
                $tags[] = $tag;
            }
        }
        if ($original->getTags() !== $tags) {
            $changes['ts'] = $tags;
        }

        if (!empty($changes)) {
            $entity->updateWithArray($changes);
        }

        $this->postRepository->save($entity);

        $response = new BaseResponse();
        $response->setPayload($entity);
        $response->triggerEvent(PostEdited::factory(null, ['original-post' => $original, 'post' => $entity, 'changes' => $changes]));

        return $response;
    }

    public function validate()
    {
        if (!$this->getCommand()->postEntity instanceof PostEntity) {
            throw new InvalidCommandArgumentException('postEntity');
        }

        if ($this->getCommand()->postEntity->isDeleted()) {
            /** @var UserEntity $deletedBy */
            $deletedBy = $this->userRepository->getById($this->getCommand()->postEntity->deletedBy());
            if ($deletedBy->getId() != $this->getCommand()->user->getId() && !$this->getCommand()->user->isAdmin()) {
                throw new DeletedByAdminException();
            }
        }

        if (!$this->getCommand()->title) {
            throw new InvalidCommandArgumentException('title');
        }

        if (!$this->getCommand()->description) {
            throw new InvalidCommandArgumentException('description');
        }

        if (!$this->getCommand()->postEntity->canEdit($this->getCommand()->user)) {
            throw new NotAuthorizedException();
        }
    }
}