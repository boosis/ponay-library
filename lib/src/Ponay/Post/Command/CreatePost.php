<?php
namespace Ponay\Ponay\Post\Command;

use Ponay\Domain\Command\Command;
use Ponay\Ponay\Post\PostEntity;
use Ponay\Ponay\Post\Poster;

class CreatePost implements Command
{
    public $author;
    public $title;
    public $shortDescription;
    public $description;
    public $tags;
    public $replyTo;
    public $publish;

    function __construct(Poster $author, $title, $shortDescription, $description, array $tags = [], PostEntity $replyTo = null, $publish = false)
    {
        $this->author = $author;
        $this->title = $title;
        $this->shortDescription = $shortDescription;
        $this->description = $description;
        $this->tags = $tags;
        $this->replyTo = $replyTo;
        $this->publish = $publish;
    }


}