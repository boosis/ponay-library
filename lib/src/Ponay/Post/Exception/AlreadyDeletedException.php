<?php
namespace Ponay\Ponay\Post\Exception;

class AlreadyDeletedException extends \Exception
{
    protected $message = 'Already deleted';
}