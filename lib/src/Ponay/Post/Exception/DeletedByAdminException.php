<?php
namespace Ponay\Ponay\Post\Exception;

class DeletedByAdminException extends \Exception
{
    protected $message = 'Deleted by Admin';
}