<?php
namespace Ponay\Ponay\Post\Event\Listener;

use Ponay\Domain\Event\Event;
use Ponay\Domain\Repository\RepositoryLocator;
use Ponay\Ponay\Post\PostEntity;

class PostReplied
{
    /** @var  RepositoryLocator */
    protected $repoManager;

    function __construct(RepositoryLocator $repoManager)
    {
        $this->repoManager = $repoManager;
    }

    public function handle(Event $event)
    {
        /** @var PostEntity $replyTo */
        $replyTo = $event->getParam('replyTo');
        $replyTo->newReplyAdded();
        $this->repoManager->get($replyTo)->save($replyTo);

        return true;
    }
}