<?php
namespace Ponay\Ponay\Post;

use Ponay\Domain\Repository\BaseRepository;
use Ponay\Ponay\Post\Enum\Status;

class PostRepository extends BaseRepository
{
    /** @var  BaseRepository */
    protected $userRepo;

    public function getPostCount($status = Status::PUBLISHED)
    {
        $condition = [];
        if (!Status::isValid($status)) {
            $status = Status::PUBLISHED;
        }
        if ($status != Status::ALL) {
            $condition['st'] = $status;
        }

        return (int) $this->getDao()->count($condition);
    }

    public function getPostCountByTags($tags, $status = Status::PUBLISHED)
    {
        foreach ($tags as &$tag) {
            $tag = trim($tag);
        }
        $condition = [
            'ts' => [
                '$all' => $tags
            ]
        ];
        if (!Status::isValid($status)) {
            $status = Status::PUBLISHED;
        }
        if ($status != Status::ALL) {
            $condition['st'] = $status;
        }

        return (int) $this->getDao()->count($condition);
    }

    public function getPostList($status = Status::PUBLISHED, $limit = 10, $skip = 0, $order = null)
    {
        $condition = [];
        if (!Status::isValid($status)) {
            $status = Status::PUBLISHED;
        }
        if ($status != Status::ALL) {
            $condition['st'] = $status;
        }
        if (!$order) {
            $order = ['crt' => -1];
        }
        $result = $this->getDao()->fetchBy($condition, [], $limit, $skip, $order);

        return $this->cursorToCollection($result);
    }

    public function getPostListByTags(array $tags, $status = Status::PUBLISHED, $limit = 10, $skip = 0, $order = null)
    {
        foreach ($tags as &$tag) {
            $tag = trim($tag);
        }
        $condition = [
            'ts' => [
                '$all' => $tags
            ]
        ];
        if (!Status::isValid($status)) {
            $status = Status::PUBLISHED;
        }
        if ($status != Status::ALL) {
            $condition['st'] = $status;
        }
        if (!$order) {
            $order = ['crt' => -1];
        }
        $result = $this->getDao()->fetchBy($condition, [], $limit, $skip, $order);

        return $this->cursorToCollection($result);
    }

    public function getPostReplies(PostEntity $post, $limit = 10, $skip = 0, $order = null)
    {
        $condition = [
            'st' => Status::PUBLISHED,
            'rt' => $post->getId()
        ];
        if (!$order) {
            $order = ['crt' => -1];
        }
        $result = $this->getDao()->fetchBy($condition, [], $limit, $skip, $order);

        return $this->cursorToCollection($result);
    }

    public function getRecentlyDiscussed($status = Status::PUBLISHED, $limit = 10, $skip = 0)
    {
        $condition = [
            'st' => $status,
        ];
        $order = ['lcon' => -1];
        $result = $this->getDao()->fetchBy($condition, [], $limit, $skip, $order);

        return $this->cursorToCollection($result);
    }

    public function getRecentlyDiscussedCount($status = Status::PUBLISHED)
    {
        $condition = [
            'st' => $status,
        ];

        return (int) $this->getDao()->count($condition);
    }

    public function getRecentlyReplied($status = Status::PUBLISHED, $limit = 10, $skip = 0)
    {
        $condition = [
            'st' => $status,
        ];
        $order = ['lron' => -1];
        $result = $this->getDao()->fetchBy($condition, [], $limit, $skip, $order);

        return $this->cursorToCollection($result);
    }

    public function getRecentlyRepliedCount($status = Status::PUBLISHED)
    {
        $condition = [
            'st' => $status,
        ];

        return (int) $this->getDao()->count($condition);
    }

    public function getUserPostCount(Poster $poster, $status = Status::PUBLISHED)
    {
        $condition = [
            'pid' => $poster->getId(),
        ];
        if (!Status::isValid($status)) {
            $status = Status::PUBLISHED;
        }
        if ($status != Status::ALL) {
            $condition['st'] = $status;
        }

        return (int) $this->getDao()->count($condition);
    }

    public function getUserPosts(Poster $poster, $status = Status::PUBLISHED, $limit = 10, $skip = 0, $order = null)
    {
        $condition = [
            'pid' => $poster->getId(),
        ];
        if (!Status::isValid($status)) {
            $status = Status::PUBLISHED;
        }
        if ($status != Status::ALL) {
            $condition['st'] = $status;
        }
        if (!$order) {
            $order = ['crt' => -1];
        }
        $result = $this->getDao()->fetchBy($condition, [], $limit, $skip, $order);

        return $this->cursorToCollection($result);
    }

    public function getUserRepo()
    {
        return $this->userRepo;
    }

    public function setUserRepo($userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function hydrateEntity($data)
    {
        /** @var PostEntity $entity */
        $entity = parent::hydrateEntity($data);
        $poster = $this->getUserRepo()->getById($entity->getPosterId());
        $entity->setTempProperty('poster', $poster);

        return $entity;
    }

}