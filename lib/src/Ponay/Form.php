<?php
namespace Ponay\Ponay;

class Form
{
    protected $isValid = true;
    protected $elements;
    protected $errors;
    protected $values;

    public function __construct(array $elements)
    {
        foreach ($elements as $element) {
            $this->elements[$element] = 1;
        }
        $this->reset();
    }

    public function getError($elementName)
    {
        if (!array_key_exists($elementName, $this->errors)) {
            throw new \InvalidArgumentException('Element [' . $elementName . '] does not exists');
        }

        return $this->errors[$elementName];
    }

    public function getValue($elementName)
    {
        if (!array_key_exists($elementName, $this->values)) {
            throw new \InvalidArgumentException('Element [' . $elementName . '] does not exists');
        }

        return $this->values[$elementName];
    }

    public function hasError($elementName)
    {
        if (!array_key_exists($elementName, $this->errors)) {
            throw new \InvalidArgumentException('Element [' . $elementName . '] does not exists');
        }
        if ($this->errors[$elementName] === null) {
            return false;
        }

        return true;
    }

    public function isValid()
    {
        return $this->isValid;
    }

    public function reset()
    {
        foreach (array_keys($this->elements) as $element) {
            $this->values[$element] = null;
            $this->errors[$element] = null;
        }
    }

    public function setError($elementName, $errorMessage)
    {
        if (!array_key_exists($elementName, $this->elements)) {
            throw new \InvalidArgumentException('Element [' . $elementName . '] does not exists');
        }
        $this->isValid = false;
        $this->errors[$elementName] = $errorMessage;
    }

    public function setValue($elementName, $value)
    {
        if (array_key_exists($elementName, $this->values)) {
            $this->values[$elementName] = $value;
        }
    }

    public function setValues(array $values)
    {
        foreach ($values as $elementName => $value) {
            $this->setValue($elementName, $value);
        }
    }
}