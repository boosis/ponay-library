<?php
namespace Ponay\Ponay\Comment\Exception;

class CommentableIsNotCommentableException extends \Exception
{
    protected $message = 'Commentable is not commentable (i.e. not published)';
}