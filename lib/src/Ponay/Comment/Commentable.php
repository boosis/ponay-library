<?php
namespace Ponay\Ponay\Comment;

interface Commentable
{
    public function getCommentCount();

    public function getId();

    public function isCommentingEnabled();

    public function newCommentAdded($time = null);
}