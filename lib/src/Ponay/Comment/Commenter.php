<?php
namespace Ponay\Ponay\Comment;

interface Commenter
{
    public function getId();
}