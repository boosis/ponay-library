<?php
namespace Ponay\Ponay\Comment\Command\Handler;

use Ponay\Domain\Command\BaseResponse;
use Ponay\Domain\Command\Command;
use Ponay\Domain\Command\Handler\AbstractHandler;
use Ponay\Domain\Repository\Repository;
use Ponay\Ponay\Comment\Command\CreateComment as CreateCommentCommand;
use Ponay\Ponay\Comment\CommentEntity;
use Ponay\Ponay\Comment\Enum\Status;
use Ponay\Ponay\Comment\Event\CommentableCommented;
use Ponay\Ponay\Comment\Exception\CommentableIsNotCommentableException;

/**
 * @method CreateCommentCommand getCommand()
 */
class CreateComment extends AbstractHandler
{
    /**
     * @var Repository
     */
    protected $commentRepository;

    public function __construct(Repository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function handle(Command $command)
    {
        $this->command = $command;
        $this->validate();
        $content = $this->formatContent($this->getCommand()->content);
        $comment = CommentEntity::factory(
            [
                'sid' => $this->getCommand()->commentable->getId(),
                'cid' => $this->getCommand()->commenter->getId(),
                'st'  => $this->getCommand()->commentableType,
                'c'   => $content,
                'sts' => Status::ACTIVE,
            ]);

        $this->commentRepository->save($comment);
        $comment->setTempProperty('commenter', $this->getCommand()->commenter);

        $response = new BaseResponse();
        $response->setPayload($comment);
        $response->triggerEvent(CommentableCommented::factory(null, ['comment' => $comment, 'commentable' => $this->getCommand()->commentable]));

        return $response;
    }

    public function validate()
    {
        if (!$this->getCommand()->commentable->isCommentingEnabled()) {
            throw new CommentableIsNotCommentableException();
        }
    }

    private function formatContent($content)
    {
        $content = preg_replace('/(\\r\\n)+/i', "\n", $content);
        $content = preg_replace('/(\\n)+/i', "\n", $content);
        $content = preg_replace('/(\\r)+/i', "\n", $content);
        $content = str_replace(["\r", "\r\n", "\n"], '<br><br>', $content);
        $content = str_replace('<br/>', '<br>', $content);
        $content = str_replace('<br />', '<br>', $content);

        return $content;
    }
}