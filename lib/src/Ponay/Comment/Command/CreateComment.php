<?php
namespace Ponay\Ponay\Comment\Command;

use Ponay\Domain\Command\Command;
use Ponay\Ponay\Comment\Commentable;
use Ponay\Ponay\Comment\Commenter;

class CreateComment implements Command
{
    /**
     * @var Commentable
     */
    public $commentable;
    /**
     * @var Commenter
     */
    public $commenter;
    public $commentableType;
    public $content;

    public function __construct(Commentable $commentable, Commenter $commenter, $commentableType, $content)
    {
        $this->commentable = $commentable;
        $this->commenter = $commenter;
        $this->commentableType = $commentableType;
        $this->content = $content;
    }


}