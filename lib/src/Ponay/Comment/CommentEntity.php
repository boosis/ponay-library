<?php
namespace Ponay\Ponay\Comment;

use Carbon\Carbon;
use Ponay\Domain\Entity\AbstractEntity;
use Ponay\Ponay\Comment\Enum\CommentableType;
use Ponay\Ponay\Comment\Enum\Status;
use Ponay\Ponay\Like\Likeable;
use Ponay\Ponay\User\UserEntity;
use Ponay\Ponay\Vote\Votable;
use Ponay\Ponay\Vote\Vote;
use Ponay\Utility\DateTime;

class CommentEntity extends AbstractEntity implements Commentable, Likeable, Votable
{
    protected $defaults = [
        'sts' => Status::ACTIVE
    ];

    public function decLikeCount()
    {
        $this->setProperty('lc', $this->getProperty('lc', 0) - 1);
    }

    public function downVote()
    {
        $votes = $this->getVoteCounts();
        $votes['d']++;
        $this->setProperty('v', $votes);
    }

    public function getCommentCount()
    {
        return $this->getProperty('cc', 0);
    }

    public function getCommenter()
    {
        return $this->getTempProperty('commenter');
    }

    public function getCommenterId()
    {
        return $this->getProperty('cid');
    }

    public function getContent()
    {
        return $this->getProperty('c');
    }

    public function getLikeCount()
    {
        return $this->getProperty('lc');
    }

    public function getRelativePublishTime()
    {
        $publishTime = $this->getProperty('crt', time());

        return DateTime::FormatTime($publishTime);
    }

    public function getSubjectId()
    {
        return $this->getProperty('sid');
    }

    public function getSubjectType()
    {
        return $this->getProperty('st');
    }

    public function getVotes()
    {
        return new Vote($this->getVoteCounts());
    }

    public function incLikeCount()
    {
        $this->setProperty('lc', $this->getProperty('lc', 0) + 1);
    }

    public function isActive()
    {
        return $this->getProperty('sts', Status::ACTIVE);
    }

    public function isCommentingEnabled()
    {
        return $this->isActive() && !$this->isReply();
    }

    public function isReply()
    {
        return $this->getSubjectType() == CommentableType::COMMENT;
    }

    public function newCommentAdded($time = null)
    {
        $this->setProperty('cc', $this->getCommentCount() + 1);
        $this->setProperty('lcon', $time && is_int($time) ? $time : Carbon::now()->timestamp);
    }

    public function toArray($includeTemps = false, $prependUnderscores = '__')
    {
        $unsavedData = $this->getUnsavedData();
        if ($includeTemps) {
            $tempData = $this->tempData;
            foreach ($tempData as $key => $value) {
                if ($value instanceof UserEntity) {
                    $value = $value->toArray();
                }
                $unsavedData[$prependUnderscores . $key] = $value;
            }
        }

        return $unsavedData;
    }

    public function upVote()
    {
        $votes = $this->getVoteCounts();
        $votes['u']++;
        $this->setProperty('v', $votes);
    }

    public function validateData($data)
    {
        // TODO: Implement validateData() method.
    }

    private function getVoteCounts()
    {
        return $this->getProperty('v', ['u' => 0, 'd' => 0]);
    }
}