<?php
namespace Ponay\Ponay\Comment\Enum;

use Ponay\Domain\Enum;

class Status extends Enum
{
    const DELETED = 'deleted';
    const ACTIVE = 'active';
}