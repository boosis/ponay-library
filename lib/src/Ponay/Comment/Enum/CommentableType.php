<?php
namespace Ponay\Ponay\Comment\Enum;

use Ponay\Domain\Enum;
use Ponay\Ponay\Comment\CommentEntity;
use Ponay\Ponay\Post\PostEntity;

class CommentableType extends Enum
{
    const POST = PostEntity::class;
    const COMMENT = CommentEntity::class;
}