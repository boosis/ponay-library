<?php
namespace Ponay\Ponay\Comment;

use Ponay\Domain\Repository\BaseRepository;
use Ponay\Ponay\Comment\Enum\CommentableType;
use Ponay\Ponay\Comment\Enum\Status;

class CommentRepository extends BaseRepository
{
    /** @var  BaseRepository */
    protected $userRepo;

    public function getPostComments($postId, $limit = 10, $skip = 0, $order = ['_id' => -1])
    {
        $condition = [
            'sid' => $postId,
            'st'  => CommentableType::POST,
            'sts' => Status::ACTIVE
        ];

        return $this->getByCondition($condition, [], $order, $limit, $skip);
    }

    public function getReplies($parentId, $fields = [], $order = ['_id' => -1], $limit = null, $skip = null)
    {
        $condition = [
            'sid' => $parentId,
            'st'  => CommentEntity::class,
            'sts' => Status::ACTIVE
        ];

        return $this->getByCondition($condition, $fields, $order, $limit, $skip);
    }

    public function getUserRepo()
    {
        return $this->userRepo;
    }

    public function setUserRepo($userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function hydrateEntity($data)
    {
        /** @var CommentEntity $entity */
        $entity = parent::hydrateEntity($data);
        $poster = $this->getUserRepo()->getById($entity->getCommenterId());
        $entity->setTempProperty('commenter', $poster);

        return $entity;
    }

}