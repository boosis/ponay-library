<?php
namespace Ponay\Ponay\Comment;

class CommentView
{
    /** @var  CommentEntity */
    protected $comment;
    /** @var  CommentRepository */
    protected $commentRepo;

    public function __construct(CommentRepository $commentRepo)
    {
        $this->commentRepo = $commentRepo;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setComment(CommentEntity $commentEntity)
    {
        $this->comment = $commentEntity;
    }

    public function getReplies($limit = 10, $skip = 0)
    {
        $replies = $this->commentRepo->getReplies($this->comment->getId(), [], ['_id' => -1], $limit, $skip);

        return $replies;
    }
}