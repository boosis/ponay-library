<?php
namespace Ponay\Ponay\Comment\Event\Listener;

use Ponay\Domain\Event\Event;
use Ponay\Domain\Repository\RepositoryLocator;
use Ponay\Ponay\Comment\Commentable;
use Ponay\Ponay\Comment\CommentEntity;

class CommentableCommented
{
    /** @var  RepositoryLocator */
    protected $repoManager;

    function __construct(RepositoryLocator $repoManager)
    {
        $this->repoManager = $repoManager;
    }

    public function handle(Event $event)
    {
        /** @var CommentEntity $comment */
        $comment = $event->getParam('comment');
        while ($comment instanceof CommentEntity) {
            $repo = $this->repoManager->getByEntityName($comment->getSubjectType());
            /** @var Commentable $subject */
            $subject = $repo->getById($comment->getSubjectId());
            if (!$subject) {
                break;
            }
            $subject->newCommentAdded();
            $repo->save($subject);
            $comment = $subject;
        }

        return true;
    }
}