<?php
namespace Ponay\Domain\Repository;

use Ponay\Domain\Entity\Entity;

interface RepositoryLocator
{
    /**
     * @param Entity $entity
     *
     * @return Repository
     */
    public function get(Entity $entity);

    /**
     * @param string $entityName
     *
     * @return Repository
     */
    public function getByEntityName($entityName);

    /**
     * @param $fullEntityClassName
     * @param $repositoryName
     *
     * @return mixed
     */
    public function set($fullEntityClassName, $repositoryName);
}