<?php
namespace Ponay\Domain\Repository;

use Ponay\Domain\Collection;
use Ponay\Domain\Dao\Dao;
use Ponay\Domain\Entity\Entity;

class BaseRepository implements Repository
{
    /** @var  Dao */
    protected $dao;
    protected $entityClassName;
    protected $entityIdField;

    private function __construct()
    {

    }

    public function count($condition)
    {
        return (int) $this->getDao()->count($condition);
    }

    public function delete($condition = [])
    {
        return $this->getDao()->deleteBy($condition);
    }

    static public function factory(Dao $dao, $entityClassName, $entityIdField)
    {
        $repository = new static();
        $repository->setDao($dao);
        $repository->setEntityClassName($entityClassName);
        $repository->setEntityIdField($entityIdField);

        return $repository;
    }

    public function getByCondition(array $condition, $fields = [], $order = [], $limit = null, $skip = null)
    {
        $result = $this->getDao()->fetchBy($condition, $fields, $limit, $skip, $order);

        return $this->cursorToCollection($result);
    }

    public function getById($id, $fields = [])
    {
        $condition = [$this->entityIdField => $id];
        $result = $this->getDao()->fetchBy($condition, $fields);
        if ($result->hasNext()) {
            return $this->hydrateEntity($result->getNext());
        }

        return false;
    }

    public function getDao()
    {
        return $this->dao;
    }

    public function setDao(Dao $dao)
    {
        $this->dao = $dao;
    }

    public function hydrateEntity($data)
    {
        $ref = new \ReflectionClass($this->entityClassName);
        $constructor = $ref->getMethod('factory');
        /** @var Entity $entity */
        $entity = $constructor->invoke(null, $data, true);

        return $entity;
    }

    public function save(Entity $entity, $options = [])
    {
        if ($entity->getId()) {
            $condition = [$this->entityIdField => $entity->getId()];
            $operation = $this->entityToOperation($entity);
            $this->getDao()->update($operation, $condition, $options);
        } else {
            $document = $this->entityToDocument($entity);
            $document = $this->getDao()->insert($document, $options);
            $entity->setId($document['_id']);
        }

        return $entity->mergeDirtyAndCommit();
    }

    public function setEntityClassName($entityClassName)
    {
        $this->entityClassName = $entityClassName;
    }

    public function setEntityIdField($entityIdField)
    {
        $this->entityIdField = $entityIdField;
    }

    protected function cursorToCollection($cursor)
    {
        $collection = new Collection();
        if ($cursor->hasNext()) {
            while ($row = $cursor->getNext()) {
                $collection->add(
                    $this->hydrateEntity($row),
                    $row[$this->entityIdField]
                );
            }
        }

        return $collection;
    }

    protected function entityToDocument(Entity $entity)
    {
        $data = $entity->getUnsavedData();
        unset($data[$this->entityIdField]);

        return $data;
    }

    protected function entityToOperation(Entity $entity)
    {
        $data = $entity->getDirtyArray();
        $operation = [];
        if (!empty($data['set'])) {
            $operation['$set'] = $data['set'];
        }
        if (!empty($data['unset'])) {
            $operation['$unset'] = $data['unset'];
        }

        return $operation;
    }
}