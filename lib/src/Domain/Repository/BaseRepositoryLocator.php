<?php
namespace Ponay\Domain\Repository;

use Ponay\Domain\Entity\Entity;
use Ponay\Domain\Repository\Exception\InvalidRepositoryDefinitionException;
use Zend\ServiceManager\ServiceLocatorInterface;

class BaseRepositoryLocator implements RepositoryLocator
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceManager;
    protected $repositories = [];
    protected $canonicalNamesReplacements = ['-' => '', '_' => '', ' ' => '', '\\' => '', '/' => ''];

    public function __construct(ServiceLocatorInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @param Entity $entity
     *
     * @return Repository
     * @throws InvalidRepositoryDefinitionException
     */
    public function get(Entity $entity)
    {
        if (!isset($this->repositories[$this->canonicalizeName(get_class($entity))])) {
            throw new InvalidRepositoryDefinitionException();
        }
        $definition = $this->repositories[$this->canonicalizeName(get_class($entity))];

        return $this->serviceManager->get($definition);
    }

    /**
     * @param string $fullEntityClassName
     *
     * @throws InvalidRepositoryDefinitionException
     * @return \Ponay\Domain\Repository\Repository
     */
    public function getByEntityName($fullEntityClassName)
    {
        if (!isset($this->repositories[$this->canonicalizeName($fullEntityClassName)])) {
            throw new InvalidRepositoryDefinitionException();
        }
        $definition = $this->repositories[$this->canonicalizeName($fullEntityClassName)];

        return $this->serviceManager->get($definition);
    }

    public function set($fullEntityClassName, $repositoryName)
    {
        $this->repositories[$this->canonicalizeName($fullEntityClassName)] = $repositoryName;
    }

    protected function canonicalizeName($name)
    {
        return strtolower(strtr($name, $this->canonicalNamesReplacements));
    }

}