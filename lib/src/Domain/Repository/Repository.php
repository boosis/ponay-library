<?php
namespace Ponay\Domain\Repository;

use Ponay\Domain\Collection;
use Ponay\Domain\Dao\Dao;
use Ponay\Domain\Entity\Entity;

interface Repository
{
    public function count($condition);

    public function delete($condition = []);

    /**
     * @param array $condition
     * @param array $fields
     *
     * @param array $order
     * @param null  $limit
     * @param null  $skip
     *
     * @return Collection
     */
    public function getByCondition(array $condition, $fields = [], $order = [], $limit = null, $skip = null);

    public function getById($id, $fields = []);

    public function getDao();

    public function hydrateEntity($data);

    public function save(Entity $entity);

    public function setDao(Dao $dao);

    public function setEntityClassName($entityClassName);

    public function setEntityIdField($entityIdField);
} 