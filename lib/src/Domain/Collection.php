<?php

namespace Ponay\Domain;

class Collection implements \IteratorAggregate, \Countable
{
    /**
     * @var array|null
     */
    protected $data = [];

    /**
     * @param null|array $items
     */
    public function __construct($items = null)
    {
        if ($items !== null && is_array($items)) {
            $this->data = $items;
        }
    }

    /**
     * @param $item
     * @param $key
     */
    public function add($item, $key)
    {
        if ($key instanceof \MongoId) {
            $key = (string) $key;
        }
        $this->data[$key] = $item;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function exists($key)
    {
        return isset($this->data[$key]);
    }

    /**
     * @return bool|mixed
     */
    public function first()
    {
        if ($this->count() > 0) {
            $data = array_values($this->data);

            return array_shift($data);
        }

        return false;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function get($key)
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return false;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Retrieve an external iterator
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return \Traversable An instance of an object implementing Iterator or
     *                      Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

    public function getNth($n)
    {
        $data = array_values($this->data);

        return $data[$n];
    }

    public function isLast($item)
    {
        $arrayKeys = array_keys($this->data);
        if (array_search($item->Label, $arrayKeys) == $this->count() - 1) {
            return true;
        }

        return false;
    }

    /**
     * @return bool|mixed
     */
    public function last()
    {
        if ($this->count() > 0) {
            $data = array_values($this->data);

            return array_pop($data);
        }

        return false;
    }

    public function merge(Collection $collection)
    {
        foreach ($collection as $key => $value) {
            $this->add($value, $key);
        }

        return $this;
    }

    /**
     * @param $key
     */
    public function remove($key)
    {
        if ($this->exists($key)) {
            unset($this->data[$key]);
        }
    }

    public function toArray()
    {
        return $this->data;
    }

    public function hasNext()
    {

    }
}
