<?php
namespace Ponay\Domain\Entity;

class IdentityGenerator
{
    protected $identifierClass;

    public function __construct($identifierClass)
    {
        $this->identifierClass = $identifierClass;
    }

    public function fromNative($native)
    {
        $reflectionMethod = new \ReflectionMethod($this->identifierClass, 'fromNative');

        return $reflectionMethod->invoke(new \ReflectionClass($this->identifierClass), $native);
    }

    public function fromString($string)
    {
        $reflectionMethod = new \ReflectionMethod($this->identifierClass, 'fromString');

        return $reflectionMethod->invoke(new \ReflectionClass($this->identifierClass), $string);
    }

    public function generate()
    {
        $reflectionMethod = new \ReflectionMethod($this->identifierClass, 'generate');

        return $reflectionMethod->invoke(new \ReflectionClass($this->identifierClass));
    }
}