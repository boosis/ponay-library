<?php
namespace Ponay\Domain\Entity;

class UnitOfWork
{
    protected $storage = [];
    protected $toBeInserted = [];
    protected $toBeUpdated = [];
    protected $toBeDeleted = [];
    protected $map = [];

    public function get($objectId)
    {
        if (isset($this->storage[$objectId])) {
            return $this->storage[$objectId];
        }

        return false;
    }

    public function getEntities()
    {
        return array_values($this->storage);
    }

    public function reset()
    {
        $this->storage = [];
        $this->map = [];
    }

    /**
     * @param      $entityId
     * @param null $entityClassName
     *
     * @return Object|bool
     */
    public function retreive($entityId, $entityClassName = null)
    {
        if ($entityId instanceof Entity) {
            $objectId = spl_object_hash($entityId);
        } else {
            $objectId = false;
            $key = $this->generateMapKey($entityId, $entityClassName);
            if (isset($this->map[$key]) && $this->map[$key]) {
                $objectId = $this->map[$key];
            }
        }
        if (!$objectId) {
            return false;
        }

        return $this->get($objectId);
    }

    public function set(Entity $entity)
    {
        $objectId = spl_object_hash($entity);
        $this->storage[$objectId] = $entity;
        $this->map[$this->generateMapKey($entity->getId(), get_class($entity))] = $objectId;
    }

    private function generateMapKey($entityId, $entityClassName)
    {
        $key = [
            'entityId'        => $entityId,
            'entityClassName' => $entityClassName
        ];

        return md5(json_encode($key));
    }
}