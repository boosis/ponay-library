<?php
namespace Ponay\Domain\Entity;

interface Entity
{
    public function getDirtyArray();

    public function getId();

    public function getProperty($name, $default = null);

    public function getUnsavedData();

    public function mergeDirtyAndCommit();

    public function setId($id);

    public function setProperty($name, $value, $toClean = false);
} 