<?php
namespace Ponay\Domain\Entity\Identity;

use Rhumsaa\Uuid\Uuid;

class UuidIdentity implements Identity
{
    /**
     * @var Uuid
     */
    protected $value;

    private function __construct(Uuid $id)
    {
        $this->value = $id;
    }

    public function __toString()
    {
        return $this->toString();
    }

    public function equals(Identity $other)
    {
        return $this->getValue() == $other->getValue();
    }

    public static function fromNative($native)
    {
        if (!$native instanceof Uuid) {
            throw new \InvalidArgumentException('Not uuid');
        }

        return new static($native);
    }

    public static function fromString($string)
    {
        return new static(Uuid::fromString($string));
    }

    public static function generate()
    {
        return new static(Uuid::uuid1());
    }

    public function getValue()
    {
        return $this->value;
    }

    public function toString()
    {
        return $this->value->toString();
    }
}