<?php
namespace Ponay\Domain\Entity\Identity;

class MongoIdIdentity implements Identity
{
    protected $value;

    private function __construct(\MongoId $id)
    {
        $this->value = $id;
    }

    public function __toString()
    {
        return $this->toString();
    }

    public function equals(Identity $other)
    {
        return $this->toString() == $other->toString();
    }

    public static function fromNative($native)
    {
        if (!$native instanceof \MongoId) {
            throw new \InvalidArgumentException('Not mongo id');
        }

        return new static($native);
    }

    public static function fromString($string)
    {
        if (\MongoId::isValid($string)) {
            return new static(new \MongoId($string));
        }
        throw new \InvalidArgumentException('Not valid mongo id');
    }

    public static function generate()
    {
        return new static(new \MongoId());
    }

    public function getValue()
    {
        return $this->value;
    }

    public function toString()
    {
        return (string) $this->value;
    }
}