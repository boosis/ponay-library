<?php
namespace Ponay\Domain\Entity\Identity;

interface Identity
{
    public function equals(Identity $other);

    public static function fromNative($native);

    public static function fromString($string);

    public static function generate();

    public function getValue();

    public function toString();
}