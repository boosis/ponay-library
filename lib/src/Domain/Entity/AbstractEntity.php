<?php
namespace Ponay\Domain\Entity;

use Ponay\Domain\Entity\Exception\RequiredFieldMissingException;

abstract class AbstractEntity implements Entity
{
    /** @var array */
    protected $data = [];
    /** @var array */
    protected $dirtyData = ['set' => [], 'unset' => []];
    /** @var array */
    protected $tempData = [];
    /** @var array */
    protected $requiredFields = [];
    /** @var array */
    protected $notEmptyFields = [];
    /** @var array */
    protected $defaults = [];
    /** @var array */
    protected $requestedFields = [];

    private function __construct()
    {
    }

    public function __get($name)
    {
        if ($name === '_id' && isset($this->data['_id'])) {
            //avoid problems with _id not being requested, but always being present nevertheless
            return $this->data['_id'];
        }
        if (strpos($name, '__') === 0) {
            return $this->getTempProperty($name);
        } else {
            if ($this->isPartial() && !in_array($name, $this->requestedFields)) {
                $this->fieldNotFound($name);
            }
            if (isset($this->dirtyData['set'][$name])) {
                return $this->dirtyData['set'][$name];
            } elseif (isset($this->dirtyData['unset'][$name])) {
                return null;
            } else {
                if (isset($this->data[$name])) {
                    return $this->data[$name];
                }
            }
        }

        return null;
    }

    public function __set($name, $value)
    {
        if (substr($name, 0, 2) == '__') {
            $this->setTempProperty($name, $value);
        } else {
            $methodName = 'set' . $name;
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            } else {
                $this->setProperty($name, $value);
            }
        }
    }

    public function __isset($name)
    {
        if ($this->isPartial() && !in_array($name, $this->requestedFields)) {
            $this->fieldNotFound($name);
        }
        if (isset($this->dirtyData['set'][$name])) {
            return true;
        } else {
            if (isset($this->data[$name])) {
                return true;
            }
        }

        return false;
    }

    public function __unset($name)
    {
        $this->dirtyData['unset'][$name] = true;
        unset($this->dirtyData['set'][$name]);
    }

    abstract function validateData($data);

    static public function factory(array $data, $clean = false)
    {
        $model = new static();
        $allData = array_merge($model->defaults, $data);
        $model->validateData($allData);

        foreach ($allData as $key => $value) {
            if (strpos($key, "__") === 0) {
                $model->tempData[$key] = $value;
                unset($allData[$key]);
            }
        }
        $model->populate($allData, $clean);

        return $model;
    }

    public function fieldNotFound($name)
    {
        if ($this->getId()) {
            throw new \Exception("You tried to access a field in model (" . get_class($this) . ") which wasn't requested from db [{$name}].");
        }
    }

    public function getDirtyArray()
    {
        return $this->dirtyData;
    }

    public function getDirtyValue($key)
    {
        if (isset($this->dirtyData['set'][$key])) {
            return $this->dirtyData['set'][$key];
        }

        return false;
    }

    public function getId()
    {
        if ($this->_id) {
            return $this->_id;
        }

        return false;
    }

    public function getProperty($name, $default = null)
    {
        return $this->{$name} ? $this->{$name} : $default;
    }

    public function getRequestedFields()
    {
        return $this->requestedFields;
    }

    public function setRequestedFields($fields)
    {
        if (!empty($fields)) {
            $arrayKeys = array_keys($fields);
            if (!is_int($arrayKeys[0])) {
                $fields = array_keys($fields);
            }
            $this->requestedFields = $fields;

        }
    }

    public function getTempProperty($name)
    {
        $name = str_replace('__', '', $name);
        if (!empty($this->tempData[$name])) {
            return $this->tempData[$name];
        }

        return null;
    }

    public function getUnsavedData()
    {
        $data = array_merge($this->data, $this->dirtyData['set']);
        foreach ($this->dirtyData['unset'] as $key => $value) {
            unset($data[$key]);
        }

        return $data;
    }

    public function isModified()
    {
        if (count($this->dirtyData['set']) > 0 || count($this->dirtyData['unset']) > 0) {
            return true;
        }

        return false;
    }

    public function isPartial()
    {
        if (empty($this->requestedFields)) {
            return false;
        }

        return true;
    }

    public function mergeDirtyAndCommit()
    {
        $this->data = $this->getUnsavedData();
        $this->resetDirty();

        return $this;
    }

    public function resetDirty()
    {
        $this->dirtyData = ['set' => [], 'unset' => []];
    }

    public function resetTemp()
    {
        $this->tempData = [];
    }

    public function setDefaults()
    {
        foreach ($this->defaults as $field => $value) {
            $this->{$field} = $value;
        }

        return $this;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function setProperty($name, $value, $toClean = false)
    {
        if ($toClean) {
            $this->data[$name] = $value;

            return $this;
        }
        $this->dirtyData['set'][$name] = $value;
        unset($this->dirtyData['unset'][$name]);

        return $this;
    }

    public function setTempProperty($name, $value)
    {
        $name = str_replace('__', '', $name);
        $this->tempData[$name] = $value;
    }

    public function toArray($includeTemps = false, $prependUnderscores = '__')
    {
        $unsavedData = $this->getUnsavedData();
        if ($includeTemps) {
            $tempData = $this->tempData;
            foreach ($tempData as $key => $value) {
                $unsavedData[$prependUnderscores . $key] = $value;
            }
        }

        return $unsavedData;
    }

    public function updateWithArray($data)
    {
        foreach ($data as $field => $value) {
            $this->setProperty($field, $value);
        }

        return $this;
    }

    public function validate()
    {
        if ($this->isPartial()) {
            throw new \Exception('Validation is not available for partial models');
        }
        $data = $this->getUnsavedData();
        if ($this->requiredFields) {
            foreach ($this->requiredFields as $fieldName) {
                if (!isset($data[$fieldName])) {
                    throw new RequiredFieldMissingException($fieldName);
                }
            }
        }
        if ($this->notEmptyFields) {
            foreach ($this->notEmptyFields as $fieldName) {
                if (empty($data[$fieldName])) {
                    throw new RequiredFieldMissingException($fieldName);
                }
            }
        }

        return true;
    }

    private function populate($data, $clean = false)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }
        if (!is_array($data)) {
            throw new \Exception('Initial data must be an array or object');
        }

        foreach ($data as $key => $value) {
            $this->data[$key] = $value;
            if (!$clean && $key != '_id') {
                $this->dirtyData['set'][$key] = $value;
            }
        }

        return $this;
    }
}