<?php
namespace Ponay\Domain\Entity;

use Ponay\Domain\Repository\RepositoryLocator;

class Manager
{
    /** @var  RepositoryLocator */
    protected $repositoryManager;
    /** @var  UnitOfWork */
    protected $unitOfWork;

    function __construct(RepositoryLocator $repositoryManager, UnitOfWork $unitOfWork)
    {
        $this->repositoryManager = $repositoryManager;
        $this->unitOfWork = $unitOfWork;
    }

    public function flush()
    {
        $entities = $this->unitOfWork->getEntities();
        foreach ($entities as $entity) {
            $this->repositoryManager->get($entity)->save($entity);
        }
        //$this->unitOfWork->reset();
    }

    public function get($id, $entityClassName, $fields = [])
    {
        if (!$entity = $this->unitOfWork->retreive($id, $entityClassName)) {
            $repo = $this->repositoryManager->getByEntityName($entityClassName);
            $entity = $repo->getById($id, $fields);
            if ($entity) {
                $this->unitOfWork->set($entity);
            }
        }

        return $entity;
    }

    public function persist(Entity $entity)
    {
        $this->unitOfWork->set($entity);
    }
}