<?php
namespace Ponay\Domain\Command\Handler;

use Ponay\Domain\Command\Command;

abstract class AbstractHandler implements Handler
{
    protected $command;

    abstract public function handle(Command $command);

    abstract public function validate();

    public function getCommand()
    {
        return $this->command;
    }

}