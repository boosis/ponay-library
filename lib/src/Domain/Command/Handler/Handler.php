<?php
namespace Ponay\Domain\Command\Handler;

use Ponay\Domain\Command\Command;

interface Handler
{
    public function getCommand();

    public function handle(Command $command);

    public function validate();
}
