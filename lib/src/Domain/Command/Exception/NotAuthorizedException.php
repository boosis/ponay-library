<?php
namespace Ponay\Domain\Command\Exception;

class NotAuthorizedException extends \Exception
{
    protected $message = 'Not authorized';
}