<?php
namespace Ponay\Domain\Command\Exception;

class InvalidCommandArgumentException extends \Exception
{
}
