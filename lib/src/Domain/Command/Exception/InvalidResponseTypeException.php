<?php
namespace Ponay\Domain\Command\Exception;

class InvalidResponseTypeException extends \Exception
{
    protected $message = 'Handler response must be \Ponay\Domain\Command\Response object';
} 