<?php
namespace Ponay\Domain\Command;

class BaseResponse implements Response
{
    protected $payload;
    /**
     * @var \SplStack
     */
    protected $eventsTriggered;

    public function getPayload()
    {
        return $this->payload;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;
    }

    public function triggerEvent($event)
    {
        $this->getTriggeredEvents()->push($event);
    }

    public function getTriggeredEvents()
    {
        if (!$this->eventsTriggered) {
            $this->eventsTriggered = new \SplStack();
        }

        return $this->eventsTriggered;
    }
}