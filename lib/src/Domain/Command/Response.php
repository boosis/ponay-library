<?php
namespace Ponay\Domain\Command;

interface Response
{
    public function getTriggeredEvents();

    public function getPayload();
} 