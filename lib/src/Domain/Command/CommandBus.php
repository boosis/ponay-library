<?php
namespace Ponay\Domain\Command;

use Closure;
use Ponay\Domain\Command\Exception\CommandHandlerNotFoundException;
use Ponay\Domain\Command\Exception\HandlerIsNotCallableException;
use Ponay\Domain\Command\Exception\InvalidResponseTypeException;
use Ponay\Domain\Event\EventDispatcher;
use Ponay\Utility\String;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\CallbackHandler;

class CommandBus
{
    /** @var ServiceLocatorInterface */
    protected $serviceLocator;
    /** @var  EventDispatcher */
    protected $eventBus;
    protected $registeredHandlers = [];

    public function __construct(ServiceLocatorInterface $serviceLocator, EventDispatcher $eventBus)
    {
        $this->serviceLocator = $serviceLocator;
        $this->eventBus = $eventBus;
    }

    public function execute(Command $command)
    {
        $handler = $this->getHandler($command);
        /** @var Response $response */
        $response = $handler->call([$command]);
        if (!$response instanceof Response) {
            throw new InvalidResponseTypeException();
        }
        $eventsTriggered = $response->getTriggeredEvents();
        foreach ($eventsTriggered as $event) {
            $this->eventBus->dispatch($event);
        }

        return $response->getPayload();
    }

    public function registerHandler($command, $handler)
    {
        $this->registeredHandlers[$this->normalizeCommandName($command)] = $handler;
    }

    /**
     * @param Command $command
     *
     * @return CallbackHandler
     * @throws CommandHandlerNotFoundException
     * @throws HandlerIsNotCallableException
     *
     * Ponay/Ponay/Like/Command/Like
     * Ponay/Ponay/Like/Command/Handler/Like
     *
     */
    protected function getHandler(Command $command)
    {
        if (isset($this->registeredHandlers[$this->normalizeCommandName(get_class($command))])) {
            $handler = $this->registeredHandlers[$this->normalizeCommandName(get_class($command))];
        } else {
            $ref = new \ReflectionClass($command);
            $commandName = $ref->getShortName();
            $handlerClassName = str_replace('Command\\' . $commandName, 'Command\\Handler\\' . $commandName, get_class($command));
            if (stristr($handlerClassName, 'Handler') === false || !class_exists($handlerClassName)) {
                throw new CommandHandlerNotFoundException('Command handler class not found: ' . $handlerClassName);
            }

            if ($this->serviceLocator->has($handlerClassName)) {
                $handler = $this->serviceLocator->get($handlerClassName);
            } else {
                $handler = new $handlerClassName();
            }
        }
        if ($handler instanceof Closure) {
            return new CallbackHandler($handler);
        } else {
            if (is_callable([$handler, 'handle'])) {
                return new CallbackHandler([$handler, 'handle']);
            }
            if ($this->serviceLocator->has($handler)) {
                return new CallbackHandler([$this->serviceLocator->get($handler), 'handle']);
            }
            throw new HandlerIsNotCallableException();
        }
    }

    protected function normalizeCommandName($name)
    {
        return String::generateSlug($name, 200, false);
    }
}
