<?php
namespace Ponay\Domain\Dao;

/**
 * @method \MongoDb getDbAdapter()
 */
class MongoDao extends AbstractDao
{
    public function count($condition = [])
    {
        return (int) $this->getDbAdapter()->selectCollection($this->getTableName())->count($condition);
    }

    public function deleteBy($condition = [])
    {
        return $this->getDbAdapter()->selectCollection($this->getTableName())->remove($condition);
    }

    public function deleteById($id)
    {
        return $this->getDbAdapter()->selectCollection($this->getTableName())->remove([$this->getIdField() => $id]);
    }

    public function fetchBy($condition = [], $fields = [], $limit = false, $skip = false, $order = [])
    {
        if (!$condition) {
            $condition = [];
        }
        $result = $this->getDbAdapter()->selectCollection($this->getTableName())->find($condition, $fields);
        if ($limit) {
            $result->limit((int) $limit);
        }
        if ($skip) {
            $result->skip($skip);
        }
        if ($order) {
            $result->sort($order);
        }

        return $result;
    }

    public function fetchById($id, $fields = [])
    {
        return $this->getDbAdapter()->selectCollection($this->getTableName())->findOne([$this->getIdField() => $id], $fields);
    }

    public function insert($document, $options = [])
    {
        if (!isset($options['w'])) {
            $options['w'] = 1;
        }

        $forceInsert = false;
        if (isset($options['forceInsert'])) {
            $forceInsert = $options['forceInsert'];
            unset($options['forceInsert']);
        }

        $id = !empty($document[$this->getIdField()]) ? $document[$this->getIdField()] : false;
        if ($id && !$forceInsert) {
            throw new \Exception('Trying to insert a model with id');
        }
        $document['crt'] = time();
        $document['upt'] = $document['crt'];
        $this->getDbAdapter()->selectCollection($this->getTableName())->insert($document, $options);

        return $document;
    }

    public function update($operation = [], $condition = [], $options = [])
    {
        if (empty($operation)) {
            return true;
            //throw new \InvalidArgumentException('$operation cannot be empty');
        }
        if (!isset($options['w'])) {
            $options['w'] = 1;
        }
        $operation['$set']['upt'] = time();

        return $this->getDbAdapter()->selectCollection($this->getTableName())->update($condition, $operation, $options);

    }
}
