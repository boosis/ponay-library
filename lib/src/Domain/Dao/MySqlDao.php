<?php
namespace Ponay\Domain\Dao;

use PDO;

/**
 * @method PDO getDbAdapter()
 */
class MySqlDao extends AbstractDao
{

    public function count($condition = [])
    {
        $statement = $this->getDbAdapter()->prepare('SELECT COUNT(*) FROM ' . $this->getTableName() . ' WHERE ' . $this->whereGenerator($condition));
        $this->getDbAdapter()->exec($statement);
    }

    private function whereGenerator($condition)
    {
        if (!$condition) {
            return '1=1';
        }
        $where = [];
        foreach ($condition as $field => $value) {
            if (is_string($value)) {
                $value = "'{$value}'";
            }
            $where[] = "{$field} = {$value}";
        }

        return implode(' AND ', $where);
    }

    public function deleteBy($condition = [])
    {
        $statement = $this->getDbAdapter()->prepare('DELETE FROM ' . $this->getTableName() . ' WHERE ' . $this->whereGenerator($condition));
        $this->getDbAdapter()->exec($statement);
    }

    public function deleteById($id)
    {
        $statement = $this->getDbAdapter()->prepare('DELETE FROM ' . $this->getTableName() . ' WHERE ' . $this->getIdField() . ' = ' . $id);
        $this->getDbAdapter()->exec($statement);
    }

    public function fetchBy($condition = [], $fields = [], $limit = null, $skip = null, $sort = null)
    {
        $statement = $this->getDbAdapter()->prepare('SELECT ' . implode(',', array_keys($fields)) . ' FROM ' . $this->getTableName() . ' WHERE ' . $this->whereGenerator($condition));
        $this->getDbAdapter()->exec($statement);
    }

    public function fetchById($id, $fields = [])
    {
        $statement = $this->getDbAdapter()->prepare('SELECT ' . implode(',', array_keys($fields)) . ' FROM ' . $this->getTableName() . ' WHERE ' . $this->getIdField() . ' = ' . $id);
        $this->getDbAdapter()->exec($statement);
    }

    public function insert($document, $options = [])
    {
        // TODO: Implement insert() method.
    }

    public function update($operation = [], $condition = [], $options = [])
    {
        // TODO: Implement update() method.
    }
}