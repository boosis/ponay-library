<?php
namespace Ponay\Domain\Dao;

use Ponay\Domain\Cursor;

interface Dao
{
    public function count($condition = []);

    public function deleteBy($condition = []);

    public function deleteById($id);

    /**
     * @param array $condition
     * @param array $fields
     * @param null $limit
     * @param null $skip
     * @param null $sort
     * @return Cursor
     */
    public function fetchBy($condition = [], $fields = [], $limit = null, $skip = null, $sort = null);

    public function fetchById($id, $fields = []);

    public function getDbAdapter();

    public function getIdField();

    public function getTableName();

    public function insert($document, $options = []);

    public function setDbAdapter($dbAdapter);

    public function setIdField($idField);

    public function setTableName($tableName);

    public function update($operation = [], $condition = [], $options = []);
}