<?php

namespace Ponay\Domain\Dao;

abstract class AbstractDao implements Dao
{
    protected $dbAdapter;
    protected $tableName;
    protected $idField;

    private function __construct()
    {
    }

    abstract public function count($condition = []);

    abstract public function deleteBy($condition = []);

    abstract public function deleteById($id);

    abstract public function fetchBy($condition = [], $fields = [], $limit = null, $skip = null, $sort = null);

    abstract public function fetchById($id, $fields = []);

    abstract public function insert($document, $options = []);

    abstract public function update($operation = [], $condition = [], $options = []);

    static public function factory($dbAdapter, $tableName, $idField)
    {
        $dao = new static();
        $dao->setDbAdapter($dbAdapter);
        $dao->setTableName($tableName);
        $dao->setIdField($idField);

        return $dao;
    }

    public function getDbAdapter()
    {
        return $this->dbAdapter;
    }

    public function setDbAdapter($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function getIdField()
    {
        return $this->idField;
    }

    public function setIdField($idField)
    {
        $this->idField = $idField;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }
}
