<?php
namespace Ponay\Domain\Dao;

use Ponay\Domain\Cursor;
use Ponay\Domain\Db\Adapter\ArrayAdapter;
use Rhumsaa\Uuid\Uuid;

/**
 * @method ArrayAdapter getDbAdapter()
 */
class ArrayDao extends AbstractDao
{
    protected $repo = [];
    protected $tableName;
    protected $idField;

    public function addToRepo($data)
    {
        $this->repo[(string) $data[$this->getIdField()]] = $data;
    }

    public function getRepo()
    {
        return $this->repo;
    }

    public function count($condition = [])
    {
        $count = 0;
        foreach ($this->getRepo() as $row) {
            $result = true;
            foreach ($condition as $key => $value) {
                if (isset($row[$key]) && $row[$key] == $value) {
                    $result = $result && true;
                } else {
                    $result = $result && false;
                }
            }
            if ($result) {
                $count++;
            }
        }

        return $count;
    }

    public function deleteBy($condition = [])
    {
        foreach ($this->getRepo() as $index => $row) {
            $result = true;
            foreach ($condition as $key => $value) {
                if (isset($row[$key]) && $row[$key] == $value) {
                    $result = $result && true;
                } else {
                    $result = $result && false;
                }
            }
            if ($result) {
                unset($this->repo[$index]);
            }
        }

        return true;
    }

    public function deleteById($id)
    {
        if (isset($this->repo[(string) $id])) {
            unset($this->repo[(string) $id]);
        }

        return true;
    }

    public function fetchBy($condition = [], $fields = [], $limit = null, $skip = null, $sort = null)
    {
        $cursor = new Cursor();
        foreach ($this->getRepo() as $row) {
            $result = true;
            foreach ($condition as $key => $value) {
                if (isset($row[$key]) && $row[$key] == $value) {
                    $result = $result && true;
                } else {
                    $result = $result && false;
                }
            }
            if ($result) {
                return $cursor->add($row);
            }
        }

        return $cursor;
    }

    public function fetchById($id, $fields = [])
    {
        if (isset($this->repo[(string) $id])) {
            return $this->repo[(string) $id];
        }

        return false;
    }

    public function insert($document, $options = [])
    {
        $document[$this->getIdField()] = (string) Uuid::uuid1();
        $this->addToRepo($document);

        return $document;
    }

    public function update($operation = [], $condition = [], $options = [])
    {
        $repo = $this->getRepo();
        $index = (string) $condition[$this->getIdField()];
        $document = $repo[$index];

        if (!empty($operation['$set'])) {
            foreach ($operation['$set'] as $field => $value) {
                $document[$field] = $value;
            }
        }
        if (!empty($operation['$unset'])) {
            foreach ($operation['$unset'] as $field => $value) {
                unset($document[$field]);
            }
        }
        $this->addToRepo($document);

        return $document;
    }
}