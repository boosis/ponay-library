<?php
namespace Ponay\Domain\ServiceLocator;

use Ponay\Domain\ServiceLocator\Exception\ServiceNotAvailableException;
use Zend\ServiceManager\ServiceLocatorInterface;

class BaseServiceLocator implements ServiceLocatorInterface
{
    protected $definitions = [];
    protected $instances = [];
    protected $shared = [];
    protected $canonicalNamesReplacements = ['-' => '', '_' => '', ' ' => '', '\\' => '', '/' => ''];

    public function has($serviceName)
    {
        return !empty($this->definitions[$this->canonicalizeName($serviceName)]);
    }

    public function get($serviceName, $parameters = [])
    {
        if (!$this->has($serviceName)) {
            throw new ServiceNotAvailableException('Service [' . $serviceName . '] is not available.');
        }
        if ($this->isShared($serviceName)) {
            if ($instance = $this->getExistingInstance($serviceName)) {
                return $instance;
            }
        }
        $definition = $this->definitions[$this->canonicalizeName($serviceName)];
        if (is_callable($definition)) {
            $service = $definition($this, $parameters);
        } else {
            $service = $definition;
        }
        if ($this->isShared($serviceName)) {
            $this->addToExistingInstances($serviceName, $service);
        }

        return $service;
    }

    public function isShared($serviceName)
    {
        return !empty($this->shared[$this->canonicalizeName($serviceName)]);
    }

    public function set($serviceName, $service, $shared = true)
    {
        $this->removeFromExistingInstances($serviceName);
        $this->definitions[$this->canonicalizeName($serviceName)] = $service;
        if ($shared) {
            $this->shared[$this->canonicalizeName($serviceName)] = true;
        }
    }

    private function getExistingInstance($serviceName)
    {
        if (isset($this->instances[$this->canonicalizeName($serviceName)]) && $this->instances[$this->canonicalizeName($serviceName)]) {
            return $this->instances[$this->canonicalizeName($serviceName)];
        }

        return false;
    }

    private function addToExistingInstances($serviceName, $service)
    {
        $this->instances[$this->canonicalizeName($serviceName)] = $service;
    }

    private function removeFromExistingInstances($serviceName)
    {
        if (isset($this->instances[$this->canonicalizeName($serviceName)])) {
            unset($this->instances[$this->canonicalizeName($serviceName)]);
        }
    }

    protected function canonicalizeName($name)
    {
        return strtolower(strtr($name, $this->canonicalNamesReplacements));
    }
}