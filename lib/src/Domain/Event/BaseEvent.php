<?php
namespace Ponay\Domain\Event;

class BaseEvent implements Event
{
    protected $name;
    protected $params = [];

    private function __construct()
    {
    }

    static public function factory($name = null, array $params = [])
    {
        $event = new static();
        $event->setName($name ? : static::class);
        $event->setParams($params);

        return $event;
    }

    public function getName()
    {
        $this->name = $this->name ? $this->name : static::class;

        return $this->name;
    }

    public function getParam($name)
    {
        return $this->params[$name];
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setParams(array $params = [])
    {
        $this->params = $params;
    }

    private function setName($name)
    {
        $this->name = $name;
    }
}