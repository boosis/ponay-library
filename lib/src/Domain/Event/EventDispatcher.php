<?php
namespace Ponay\Domain\Event;

use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\CallbackHandler;
use Zend\Stdlib\PriorityQueue;
use Zend\Stdlib\SplStack;

class EventDispatcher
{
    /**
     * @var array
     */
    protected $eventListeners = [];
    protected $responses;

    public function addListener($eventName, $callback, $priority = 100)
    {
        if ($eventName instanceof Event) {
            $eventName = $eventName->getName();
        }
        if (empty($this->eventListeners[$eventName])) {
            $this->eventListeners[$eventName] = new PriorityQueue();
        }
        $listener = new CallbackHandler($callback, ['event' => $eventName, 'priority' => $priority]);

        /** @var PriorityQueue $listeners */
        $listeners = $this->eventListeners[$eventName];
        $listeners->insert($listener, $priority);
    }

    public function dispatch($event, $eventParams = [])
    {
        if (!$event instanceof Event) {
            $event = BaseEvent::factory($event);
        }
        $event->setParams(ArrayUtils::merge($event->getParams(), $eventParams));
        /** @var PriorityQueue $listeners */
        $listeners = $this->getListeners($event->getName());
        $wildcardListeners = $this->getListeners('*');
        $listeners = $this->mergeListeners($listeners, $wildcardListeners);
        /** @var CallBackHandler $listener */
        foreach ($listeners as $listener) {
            $listenerCallback = $listener->getCallback();
            $this->getReponses()->push(call_user_func($listenerCallback, $event));
        }

        return $this->getReponses();
    }

    public function getListeners($eventName)
    {
        if (empty($this->eventListeners[$eventName])) {
            $this->eventListeners[$eventName] = new PriorityQueue();
        }

        return $this->eventListeners[$eventName];
    }

    public function getReponses()
    {
        if (!$this->responses) {
            $this->responses = new SplStack();
        }

        return $this->responses;
    }

    private function mergeListeners(PriorityQueue $masterListeners, PriorityQueue $listeners)
    {
        /** @var CallbackHandler $listener */
        foreach ($listeners as $listener) {
            $priority = $listener->getMetadatum('priority');
            if (null === $priority) {
                $priority = 1;
            } elseif (is_array($priority)) {
                $priority = array_shift($priority);
            }
            $masterListeners->insert($listener, $priority);
        }

        return $masterListeners;
    }
}