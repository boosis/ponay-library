<?php
namespace Ponay\Domain\Event;

interface Event
{
    public function getName();

    public function getParams();

    public function setParams(array $params = []);

    public function getParam($name);
} 