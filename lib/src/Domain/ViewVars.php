<?php
namespace Ponay\Domain;

class ViewVars implements \ArrayAccess
{
    protected $vars = [];

    public function __construct($vars = [])
    {
        $this->vars = $vars;
    }

    public function __get($key)
    {
        if (isset($this->vars[$key])) {
            return $this->vars[$key];
        }

        throw new \Exception('Key [' . (string)$key . '] not found');
    }

    public function __set($key, $value)
    {
        $this->offsetSet($key, $value);
    }

    public function get($key, $default = null)
    {
        return $this->offsetGet($key) ? $this->offsetGet($key) : $default;
    }

    public function offsetExists($offset)
    {
        return isset($this->vars[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->vars[$offset]) ? $this->vars[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        $this->vars[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->vars[$offset]);
    }

    public function set($key, $value)
    {
        $this->offsetSet($key, $value);
    }
}